using System;
using System.Collections.Generic;
using UnityEngine;

public static class IAController {
    private static int unitIndex { get; set; }

    public static bool Turn(Game g, List<Unit> units, Maps m)
    {
        if (g.battle || unitIndex >= units.Count)
        {
            unitIndex = 0;
            return false;
        }

        Unit u = units[unitIndex];

        unitIndex++;
    
        Unit target = GetNearestPlayer(u, m);

        if (target == null)
            return true;

        if (!IsInRange(m, u, target))
            return true;

        List<Tile> walkables = m.Movement(u);

        Tile targetTile = GetNearestTileToUnit(m, walkables, target);

        if (targetTile != null)
        {
            g.RPCMove(u.GetId(), targetTile.GetPos().x, targetTile.GetPos().y);
            Debug.Log("Moved AI unit " + u.GetId() + " to " + targetTile.GetPos());
        }

        g.RPCAttack(u.GetId(), target.GetId());

        return true;
    }

    private static Unit GetNearestPlayer(Unit u, Maps m) {
        List<Unit> players = m.GetTeam1();

        Unit target = Outils.GetNearest(u, players);

        return target;
    }

    private static bool IsInRange(Maps m, Unit u, Unit target) {
        Tile curr = m.FindTile(u.GetPos());

        int range = u.GetDetect_range();

        List<Tile> rangeTiles = m._Range(curr, range);

        bool res = false;

        foreach (Tile t in rangeTiles) {
            if (t.GetPos() == target.GetPos()) {
                res = true;
                break;
            }
        }

        return res;
    }

    private static Tile GetNearestTileToUnit(Maps m, List<Tile> tiles, Unit target) {
        int dist = Int32.MaxValue;
        Tile nearest = null;

        Vector2 targetPos = target.GetPos();

        foreach (Tile t in tiles) {
            Vector2 tPos = t.GetPos();

            int newDist = (int) Outils.Distance(targetPos, tPos);

            if (newDist < dist) {
                dist = newDist;
                nearest = t;
            }
        }

        return nearest;
    }
}