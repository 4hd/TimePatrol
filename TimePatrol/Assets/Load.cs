﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Load : MonoBehaviour {
    public static bool load = false;
    public static Maps m;
    public static int scene;

    public GameObject text;

    void Start() {
        Debug.Log("Starting loading");
        
        string[] gameSave = new[] {""};
        if (File.Exists("Game Save.sav")) {
            gameSave = Outils.FileToString("Game Save.sav");
            Debug.Log("Game Save.sav trouve");
        }

        
        if (gameSave != null) {
            load = true;
            Debug.Log("gameSave pas vide");
            
            Debug.Log("Loading");
            
            int i = 0;
            int length = gameSave.Length;
            string map = gameSave[0];
            List<Unit> units1 = new List<Unit>();
            List<Unit> units2 = new List<Unit>();
            while (gameSave[i][0] != '@')
                i++;
            i++;

            while (gameSave[i][0] != '#') {
                Unit u;
                string[] info = gameSave[i].Split(':');
                switch (info[0]) {
                    case "Knight":
                        u = new Melee.Knight();
                        Debug.Log("Creating new Knight");
                        break;
                    case "Wizard":
                        u = new Special.Wizard();
                        Debug.Log("Creating new Wizard");
                        break;
                    case "Ursari":
                        u = new Tank.Ursari();
                        Debug.Log("Creating new Ursari");
                        break;
                    case "CrossbowMan":
                        u = new Range.CrossbowMan();
                        Debug.Log("Creating new Crossbow");
                        break;

                    case "Mecha":
                        u = new Melee.Mecha();
                        Debug.Log("Creating new Mecha");
                        break;
                    case "Cyborg":
                        u = new Special.Cyborg();
                        Debug.Log("Creating new Cyborg");
                        break;
                    case "AssaultTank":
                        u = new Tank.AssaultTank();
                        Debug.Log("Creating new Tank");
                        break;
                    case "RobotSniper":
                        u = new Range.RobotSniper();
                        Debug.Log("Creating new Sniper");
                        break;

                    case "Infantry":
                        u = new Melee.Infantry();
                        Debug.Log("Creating new Infantry");
                        break;
                    case "Medic":
                        u = new Special.Medic();
                        Debug.Log("Creating new Medic");
                        break;
                    case "AntiRiot":
                        u = new Tank.AntiRiot();
                        Debug.Log("Creating new Riot");
                        break;
                    case "Gunner":
                        u = new Range.Gunner();
                        Debug.Log("Creating new Gunner");
                        break;

                    default:
                        u = new Melee.Knight();
                        new ParserFailure("les unites ne sont pas formate correctement");
                        break;
                }

                int n;
                if (int.TryParse(info[1], out n)) {
                    u.SetHealth(n);
                    Debug.Log("HP:" + n);
                }
                else {
                    new ParserFailure("les pv ne sont pas formate correctement");
                }

                if (int.TryParse(info[2], out n)) {
                    int y;

                    if (int.TryParse(info[3], out y)) {
                        u.SetPos(n, y);
                        Debug.Log("Pos:" + n+":"+y);
                    }
                    else {
                        new ParserFailure("les pos ne sont pas formate correctement");
                    }
                }
                else {
                    new ParserFailure("les pos ne sont pas formate correctement");
                }

                i++;

                units1.Add(u);
            }

            i++;

            while (i < length) {
                Unit u;
                string[] info = gameSave[i].Split(':');
                switch (info[0]) {
                    case "Knight":
                        u = new Melee.Knight();
                        break;
                    case "Wizard":
                        u = new Special.Wizard();
                        break;
                    case "Ursari":
                        u = new Tank.Ursari();
                        break;
                    case "CrossbowMan":
                        u = new Range.CrossbowMan();
                        break;

                    case "Mecha":
                        u = new Melee.Mecha();
                        break;
                    case "Cyborg":
                        u = new Special.Cyborg();
                        break;
                    case "AssaultTank":
                        u = new Tank.AssaultTank();
                        break;
                    case "RobotSniper":
                        u = new Range.RobotSniper();
                        break;

                    case "Infantry":
                        u = new Melee.Infantry();
                        break;
                    case "Medic":
                        u = new Special.Medic();
                        break;
                    case "AntiRiot":
                        u = new Tank.AntiRiot();
                        break;
                    case "Gunner":
                        u = new Range.Gunner();
                        break;

                    default:
                        u = new Melee.Knight();
                        new ParserFailure("les unites ne sont pas formate correctement");
                        break;
                }

                int n;
                if (int.TryParse(info[1], out n)) {
                    u.SetHealth(n);
                }
                else {
                    new ParserFailure("les pv ne sont pas formate correctement");
                }

                if (int.TryParse(info[2], out n)) {
                    int y;

                    if (int.TryParse(info[3], out y)) {
                        u.SetPos(n, y);
                    }
                    else {
                        new ParserFailure("les pos ne sont pas formate correctement");
                    }
                }
                else {
                    new ParserFailure("les pos ne sont pas formate correctement");
                }

                i++;
                units2.Add(u);
            }

            m = new Maps(Application.streamingAssetsPath + "/Levels/" + map + ".lvl", units1, units2);

            scene = int.Parse(map.Split('_')[1][0] + "");
            LoadSceneOnClick.LoadByIndex(scene + 4);
        }
        else {
            Debug.Log("Peut pas charger");
            text.SetActive(true);
        }
    }
}

public class ParserFailure : Exception {
    public ParserFailure(string message) : base(message) {
    }
}