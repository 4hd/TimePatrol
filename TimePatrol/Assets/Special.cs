public class Special : Unit {
    public class Wizard : Special {
        public Wizard() {
            health = 50;
            attack = 60;
            defense = 10;
            mobility = 3;
            attack_range = 2;
            speed = 90;
            dodge = 5;
            luck = 5;
        }
    }

    public class Medic : Special {
        public Medic() {
            health = 60;
            attack = 20;
            defense = 10;
            mobility = 4;
            attack_range = 1;
            heal = 42;
            speed = 90;
            dodge = 10;
            luck = 10;
        }
    }

    public class Cyborg : Special {
        public Cyborg() {
            health = 90;
            attack = 65;
            defense = 22;
            mobility = 4;
            attack_range = 2;
            speed = 100;
            dodge = 25;
            luck = 20;
        }
    }
}