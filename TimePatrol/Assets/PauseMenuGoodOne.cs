﻿using UnityEngine;

public class PauseMenuGoodOne : MonoBehaviour
{

    public static bool gameIsPaused = false;

    public GameObject PauseMenuUI;
    
    // Update is called once per frame
    void Update()
    {
        if (!EndGame.End_Screen)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (gameIsPaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }    
        }
    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    void Pause()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;

    }

    public void Quit() {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
        LoadSceneOnClick.LoadByIndex(0);
    }
}
