using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class Game {
    public Maps m;

    private Dictionary<GameObject, Unit> units;
    private Unit selectedUnit;
    private Vector2Int selectedPos;
    private Tile selectedTile;

    private GameObject cursor;
    private GameObject walkable;
    private GameObject attackable;
    private GameObject moved;
    private GameObject attacked;
    private List<GameObject> walkableObjects;
    private List<Tile> walkableTiles;
    private List<GameObject> attackableObjects;
    private List<Tile> attackTiles;
    private List<GameObject> movedObjects;
    private List<GameObject> attackedObjects;
    private GameObject texts;
    private GameObject hud;
    private GameObject mouseCollider;
    private GameObject Battle_BG;

    private bool playerTurn;
    private HashSet<Unit> alreadyMoved;
    private HashSet<Unit> alreadyAttacked;
    public Unit attacker;

    private bool multiplayer;

    public bool battle;
    public int battleI;
    public int[] battleInfo;
    public Unit U1;
    public Unit U2;
    private GameObject goU1;
    private GameObject goU2;

    private GameObject battleCam;
//    public StartMap sm;

    public Unit RPCAttacker;
    public Unit RPCDefender;
    public Vector2Int RPCMovePos;
    public bool RPCSwitchTurn;

    float vAxis;
    float hAxis;
    private static Dictionary<string, int> inputHeld;

    private int i;

    public Game(Maps m, Dictionary<GameObject, Unit> units, GameObject cursor, GameObject walkable,
        GameObject attackable, GameObject texts, GameObject hud, bool playerTurn, bool multiplayer,
        GameObject moved, GameObject attacked, GameObject mouseCollider) {
        this.m = m;
        this.units = units;
        this.multiplayer = multiplayer;

        selectedPos.x = (int) cursor.transform.position.x;
        selectedPos.y = (int) cursor.transform.position.z * -1;
        selectedTile = m.FindTile(selectedPos);
        //selectedTile = m.FindTile(new Vector2Int(m.GetWidth() / 2, m.GetHeight() / 2));
        this.cursor = cursor;
        this.walkable = walkable;
        this.attackable = attackable;
        this.moved = moved;
        this.attacked = attacked;
        this.texts = texts;
        this.hud = hud;
        this.mouseCollider = mouseCollider;
        walkableObjects = new List<GameObject>();
        attackableObjects = new List<GameObject>();
        movedObjects = new List<GameObject>();
        attackedObjects = new List<GameObject>();

        battleCam = GameObject.Find("/Main/Battle/Camera");
        battleCam.SetActive(false);
        Battle_BG = GameObject.Find("/Main/Battle/Battle_BG");
        Battle_BG.SetActive(false);

        inputHeld = new Dictionary<string, int>
        {
            {"vAxis", 0},
            {"hAxis", 0},
            {"select", 0},
            {"unselect", 0},
            {"attack", 0},
            {"endTurn", 0},
            {"camera", 0}
        };

        if (playerTurn) {
            StartTurn();
        }
        else {
            EndTurn();
        }
    }

    public void Update() {
        RPCAttacker = null;
        RPCDefender = null;
        RPCMovePos = new Vector2Int(-1, -1);
        RPCSwitchTurn = false;

        if (!PauseMenuGoodOne.gameIsPaused) {
            if (!battle) {
                if (playerTurn) {
                    vAxis = Input.GetAxisRaw("VerticalArrow");
                    hAxis = Input.GetAxisRaw("HorizontalArrow");
                    if (Input.GetMouseButtonDown(0)) {
                        mouseCollider.SetActive(true);
                    }
                    else {
                        mouseCollider.SetActive(false);
                    }

                    
                    
                    MoveCursor();
                    MouseControls();
                    KeyboardControls();
                    if ((m.GetTeam1().Count == alreadyAttacked.Count || PositiveEdge("endTurn")) && !battle) {
                        RPCSwitchTurn = true;
                        EndTurn();
                    }
                }
                else {
                    if (m.GetTeam2().Count == 0)
                    {

                        EndGame.victory = true;
                        
                        if (!multiplayer) {
                            Save.LevelSaver(SceneManager.GetActiveScene().buildIndex - 4); //TODO changer num niveau
                        }
                    }
                    else if (!multiplayer) {
                        IA();
                    }
                }
            }
            else {
                if (PositiveEdge("select")) {
                    battleI = -1;
                }

                if (i * Time.deltaTime >= 1) {
                    BattleAnimation(U1, U2);
                    i = 0;
                }

                i++;
            }
        }
    }


    //deselectionne, selection et deplacement unite avec souris
    public void MouseControls() {
        if (Input.GetMouseButtonDown(0)) {
            Vector2Int oldPos = selectedPos;
            if (Camera.main != null)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 1000)) {
                    selectedPos = new Vector2Int((int) (hit.point.x + 0.5f), (int) -(hit.point.z - 0.5f));
                    selectedTile = m.FindTile(selectedPos);
                }
            }

            if (selectedPos == oldPos) {
                SelectMoveUnit();
            }
        }

        if (Input.GetMouseButtonDown(1)) {
            UnselectUnit();
        }
    }

    //deselectionne, selection et deplacement unite avec clavier
    public void KeyboardControls() {
        if (PositiveEdge("unselect")) {
            UnselectUnit();
        }

        if (PositiveEdge("select")) {
            SelectMoveUnit();
        }
        else if (PositiveEdge("attack")) {
            if (selectedUnit != null && !alreadyMoved.Contains(selectedUnit)) {
                attacker = selectedUnit;
                selectedUnit = null;
                alreadyMoved.Add(attacker);

                DisplayRange(attacker);

                GameObject m2 = Object.Instantiate(moved,
                    new Vector3(attacker.GetPos().x, 0.5f, -attacker.GetPos().y),
                    walkable.transform.rotation);

                attacker.effect = m2;
                movedObjects.Add(m2);

                //supprime les carres verte
                foreach (GameObject go in walkableObjects) {
                    UnityEngine.Object.Destroy(go);
                }
            }
        }
    }

    //deplacement curseur clavier
    public void MoveCursor() {
        bool needUpdate = false;

        //actualise dico
        if (0 >= vAxis && vAxis >= 0) {
            inputHeld["vAxis"] = 0;
        }
        else {
            inputHeld["vAxis"] += 1;
        }

        if (0 >= hAxis && hAxis >= 0) {
            inputHeld["hAxis"] = 0;
        }
        else {
            inputHeld["hAxis"] += 1;
        }

        //touche enfonce
        if (inputHeld["vAxis"] > 30 && inputHeld["vAxis"] % 2 == 0) {
            if (vAxis > 0f) {
                selectedPos.y -= 1;
            }
            else {
                selectedPos.y += 1;
            }

            needUpdate = true;
        }
        else {
            //positive edge
            if (inputHeld["vAxis"] == 1) {
                if (vAxis > 0f) {
                    selectedPos.y -= 1;
                }
                else {
                    selectedPos.y += 1;
                }

                needUpdate = true;
            }
        }

        //touche enfonce
        if (inputHeld["hAxis"] > 30 && inputHeld["hAxis"] % 2 == 0) {
            if (hAxis > 0f) {
                selectedPos.x += 1;
            }
            else {
                selectedPos.x -= 1;
            }

            needUpdate = true;
        }
        else {
            //positive edge
            if (inputHeld["hAxis"] == 1) {
                if (hAxis > 0f) {
                    selectedPos.x += 1;
                }
                else {
                    selectedPos.x -= 1;
                }
            }

            needUpdate = true;
        }

        if (needUpdate) {
            ChangeSelected();
        }
    }

    //changement selection case clavier
    public void ChangeSelected() {
        //TODO faire avec modulo?
        if (selectedPos.x >= m.GetWidth()) {
            selectedPos.x -= m.GetWidth();
        }

        if (selectedPos.x < 0) {
            selectedPos.x += m.GetWidth();
        }

        if (selectedPos.y >= m.GetHeight()) {
            selectedPos.y -= m.GetHeight();
        }

        if (selectedPos.y < 0) {
            selectedPos.y += m.GetHeight();
        }

        selectedTile = m.FindTile(selectedPos);
        cursor.transform.position = new Vector3(selectedPos.x, cursor.transform.position.y, -selectedPos.y);
    }

    public static bool PositiveEdge(string key) {
        if (Math.Abs(Input.GetAxisRaw(key)) < 0.001f) {
            inputHeld[key] = 0;
        }
        else {
            inputHeld[key] += 1;
        }

        return inputHeld[key] == 1;
    }

    //gere deplacement et attaque
    public void SelectMoveUnit() {
        if (attacker == null) {
            if (selectedTile.GetUnit() != null) {
                //selection unite
                selectedUnit = selectedTile.GetUnit();
                if (!alreadyMoved.Contains(selectedUnit))
                    DisplayWalkable();
                else {
                    if (!alreadyAttacked.Contains(selectedUnit)) {
                        attacker = selectedUnit;
                        DisplayRange(attacker);
                    }
                }

                DisplayInfo(selectedUnit);
            }

            else if (selectedUnit != null) {
                //deplacement unite
                //if (selectedUnit.IsPlayer() && !alreadyMoved.Contains(selectedUnit)) {
                if (selectedUnit.IsPlayer()) {
                    if (!alreadyMoved.Contains(selectedUnit) && m.ChangeUnitPos(selectedUnit, selectedTile)) {
                        // for multiplayer
                        RPCMovePos = selectedTile.GetPos();
                        RPCAttacker = selectedUnit;

                        //supprime les carres vertes
                        foreach (GameObject go in walkableObjects) {
                            UnityEngine.Object.DestroyImmediate(go);
                        }

                        alreadyMoved.Add(selectedUnit);
                        
                        GameObject m2 = Object.Instantiate(moved,
                            new Vector3(selectedUnit.GetPos().x, 0.5f, -selectedUnit.GetPos().y),
                            walkable.transform.rotation);
                        
                        selectedUnit.effect = m2;
                        movedObjects.Add(m2);

                        attacker = selectedUnit;
                        
                        DisplayRange(attacker);

                        selectedUnit = null;
                    }
                }
                else {
                    if (alreadyMoved.Contains(selectedUnit) && selectedUnit.IsPlayer()) {
                        attacker = selectedUnit;
                        DisplayRange(attacker);
                        selectedUnit = null;
                    }
                }
            }
        }

        if (attacker == null) return;
        
        DisplayInfo(attacker);
            
        if (selectedTile.GetUnit() != null) {
            selectedUnit = selectedTile.GetUnit();

            if (multiplayer && RPCMovePos.x == -1 && RPCMovePos.y == -1)
            {
                RPCAttacker = attacker;
                RPCDefender = selectedUnit;
                    
                Debug.LogError("Sending RPC attack data");
            }
                
            Attack(selectedUnit);
        }
        else {
            Debug.Log("No unit selected");
        }
    }


    //deselection
    public void UnselectUnit() {
        selectedUnit = null;
        attacker = null;

        hud.SetActive(false);

        foreach (GameObject go in walkableObjects) {
            UnityEngine.Object.DestroyImmediate(go);
        }

        foreach (GameObject ob in attackableObjects) {
            UnityEngine.Object.DestroyImmediate(ob);
        }
    }

    public void StartTurn() {
        if (m.GetTeam1().Count <= 0)
            EndGame.defeat = true;
        
        playerTurn = true;
        alreadyMoved = new HashSet<Unit>();
        alreadyAttacked = new HashSet<Unit>();
        selectedUnit = null;
        attacker = null;
        foreach (GameObject go in movedObjects) {
            UnityEngine.Object.DestroyImmediate(go);
        }

        foreach (GameObject go in attackedObjects) {
            UnityEngine.Object.DestroyImmediate(go);
        }
    }

    public void EndTurn() {
        playerTurn = false;
        selectedUnit = null;
        attacker = null;
        if (!multiplayer)
            Save.GameSaver(this);

        foreach (GameObject go in attackableObjects) {
            UnityEngine.Object.DestroyImmediate(go);
        }

        foreach (GameObject go in walkableObjects) {
            UnityEngine.Object.DestroyImmediate(go);
        }

        foreach (GameObject go in movedObjects) {
            UnityEngine.Object.DestroyImmediate(go);
        }

        foreach (GameObject go in attackedObjects) {
            UnityEngine.Object.DestroyImmediate(go);
        }

        DisplayInfo(null);
    }

    public void Attack(Unit def) {
        if (!alreadyAttacked.Contains(attacker)) {
            if (m.Range(attacker).Contains(m.FindTile(def.GetPos())) && attacker != def) {
                bool crit;
                battleInfo = new int[5];

                //attaquant
                if (Battle.Double_hit(attacker, def, m)) {
                    crit = Battle.Crit(attacker);
                    //info combat pour anim
                    if (crit) {
                        battleInfo[1] = 2;
                    }
                    else {
                        battleInfo[1] = 1;
                    }

                    if (Battle.Combat(attacker, def, crit, m)) {
                        Debug.Log("Touche");
                    }
                    else {
                        Debug.Log("Rate");
                        battleInfo[1] = -1; //info combat pour anim
                    }

                    Debug.Log("Crit: " + crit);
                }

                crit = Battle.Crit(attacker);
                if (crit) {
                    battleInfo[0] = 2;
                }
                else {
                    battleInfo[0] = 1;
                }

                if (Battle.Combat(attacker, def, crit, m)) {
                    Debug.Log("Touche");
                }
                else {
                    battleInfo[0] = -1; //info combat pour anim
                    Debug.Log("Rate");
                }

                Debug.Log("Crit: " + crit);


                if (def.GetHealth() <= 0) {
                    Kill(def);
                    battleInfo[4] = 1;
                }
                else {
                    //defenseur
                    if (m.Range(def).Contains(m.FindTile(attacker.GetPos()))) {
                        if (Battle.Double_hit(def, attacker, m)) {
                            crit = Battle.Crit(def);

                            //info combat pour anim
                            if (crit) {
                                battleInfo[3] = 2;
                            }
                            else {
                                battleInfo[3] = 1;
                            }

                            if (Battle.Combat(def, attacker, crit, m)) {
                                Debug.Log("Touche");
                            }
                            else {
                                Debug.Log("Rate");
                                battleInfo[3] = -1; //info combat pour anim
                            }

                            Debug.Log("Crit: " + crit);
                        }

                        crit = Battle.Crit(def);
                        //info combat pour anim
                        if (crit) {
                            battleInfo[2] = 2;
                        }
                        else {
                            battleInfo[2] = 1;
                        }

                        if (Battle.Combat(def, attacker, crit, m)) {
                            Debug.Log("Touche");
                        }
                        else {
                            Debug.Log("Rate");
                            battleInfo[2] = -1; //info combat pour anim
                        }
                    }
                }

                alreadyAttacked.Add(attacker);
                GameObject a = GameObject.Instantiate(attacked,
                    new Vector3(attacker.GetPos().x, 0.5f, -attacker.GetPos().y), attacked.transform.rotation);
                attackedObjects.Add(a);
                if (attacker.effect != null) {
                    UnityEngine.Object.Destroy(attacker.effect);
                }

                attacker.effect = a;

                foreach (GameObject go in attackableObjects) {
                    UnityEngine.Object.DestroyImmediate(go);
                }

                if (attacker.GetHealth() <= 0) {
                    Kill(attacker);
                }


                battleI = 0;
                battle = true;
                BattleAnimation(attacker, def);
                attacker = null;
            }
        }
        else {
            Debug.Log("Unite a deja attaque");
        }
    }

    public void Kill(Unit u) {
        m.FindTile(u.GetPos()).SetUnit(null);
        if (u.IsPlayer() && m.GetTeam1().Contains(u)) {
            m.GetTeam1().Remove(u);
        }

        if (!u.IsPlayer() && m.GetTeam2().Contains(u)) {
            m.GetTeam2().Remove(u);
        }

        u.GetModel().SetActive(false);
        UnityEngine.Object.Destroy(u.GetModel());
        UnityEngine.Object.Destroy(u.effect);
    }

    public bool DisplayInfo(Unit u) {
        if (u != null) {
            hud.SetActive(true);
            List<GameObject> textsList = new List<GameObject>();
            for (int i = 0; i < texts.transform.childCount; i++) {
                textsList.Add(texts.transform.GetChild(i).gameObject);
            }

            foreach (GameObject go in textsList) {
                switch (go.name) {
                    case "Health_txt":
                        go.GetComponent<Text>().text = "HP : " + u.GetHealth();
                        break;
                    case "Attack_txt":
                        go.GetComponent<Text>().text = "ATK : " + u.GetAttack();
                        break;
                    case "Defense_txt":
                        go.GetComponent<Text>().text = "DEF : " + u.GetDefense();
                        break;
                    case "Speed_txt":
                        go.GetComponent<Text>().text = "SPD : " + u.GetSpeed();
                        break;
                    case "Dodge_txt":
                        go.GetComponent<Text>().text = "DGE : " + u.GetDodge();
                        break;
                    case "Unit_txt":
                        go.GetComponent<Text>().text = "" + ("" + u.GetType()).Split('+')[1];
                        break;
                    default:
                        Debug.Log("Erreur");
                        break;
                }
            }

            return true;
        }
        else {
            hud.SetActive(false);
            GameObject[] textsList = texts.gameObject.GetComponents<GameObject>();
            foreach (GameObject go in textsList) {
                switch (go.name) {
                    case "Health_txt":
                        go.GetComponent<Text>().text = "HP :";
                        break;
                    case "Attack_txt":
                        go.GetComponent<Text>().text = "ATK :";
                        break;
                    case "Defense_txt":
                        go.GetComponent<Text>().text = "DEF :";
                        break;
                    case "Speed_txt":
                        go.GetComponent<Text>().text = "SPD :";
                        break;
                    case "Dodge_txt":
                        go.GetComponent<Text>().text = "DGE :";
                        break;
                    case "Unit_txt":
                        go.GetComponent<Text>().text = " ";
                        break;
                }
            }

            return false;
        }
    }

    public void DisplayWalkable() {
        foreach (GameObject go in walkableObjects) {
            UnityEngine.Object.DestroyImmediate(go);
        }

        if (selectedUnit != null) {
            walkableTiles = m.Movement(selectedUnit);
            foreach (Tile t in walkableTiles) {
                GameObject w = UnityEngine.GameObject.Instantiate(
                    walkable, new Vector3(t.GetPos().x, 0.5f, -t.GetPos().y), walkable.transform.rotation);
                walkableObjects.Add(w);
            }
        }
    }

    public void DisplayRange(Unit u) {
        foreach (GameObject ob in attackableObjects) {
            UnityEngine.Object.DestroyImmediate(ob);
        }

        foreach (GameObject go in walkableObjects) {
            UnityEngine.GameObject.Destroy(go);
        }

        attackTiles = m.Range(u);
        foreach (Tile t in attackTiles) {
            GameObject w = UnityEngine.GameObject.Instantiate(
                attackable, new Vector3(t.GetPos().x, 0.5f, -t.GetPos().y), attackable.transform.rotation);
            attackableObjects.Add(w);
        }
    }

    public void IA() {
        bool ai = IAController.Turn(this, m.GetTeam2(), m);

        if (!ai)
        {
            if (m.GetTeam1().Count != 0)
            {
                StartTurn();
            }
            else
            {
                EndGame.defeat = true;
            }
        }
    }

    public void RPCMove(int unitID, int x, int y) {
        Unit selectedUnit = FindUnit(unitID);
        Tile targetTile = m.FindTile(new Vector2Int(x, y));

        if (!m.ChangeUnitPos(FindUnit(unitID), targetTile))
        {
            Debug.LogError("Could not move unit from remote call.");
        }
        
    }

    public void RPCAttack(int atk, int def) {
        attacker = FindUnit(atk);
        Unit d = FindUnit(def);
        
        Attack(d);
    }

    public Unit FindUnit(int id) {
        foreach (Unit u in m.GetTeam1()) {
            if (id == u.GetId()) {
                return u;
            }
        }

        foreach (Unit u in m.GetTeam2()) {
            if (id == u.GetId()) {
                return u;
            }
        }
        
        Debug.LogError("FindUnit: unit not found");

        return null;
    }

    public bool IsMultiplayer() {
        return multiplayer;
    }

    public void SwitchTurn() {
        if (playerTurn)
            EndTurn();
        else
            StartTurn();
    }

    public void BattleAnimation(Unit u1, Unit u2) {
        switch (battleI) {
            case 0:
                battle = true;
                Battle_BG.SetActive(true);
                CameraController.cameras[CameraController.currentCameraIndex].gameObject.SetActive(false);
                battleCam.SetActive(true);
                if (goU1 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU1);
                }

                if (goU2 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU2);
                }

                U1 = u1;
                goU1 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral"),
                    new Vector3(-750, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral")
                        .transform.rotation);
                goU1.transform.localScale = new Vector3(10, 10, 1);

                U2 = u2;
                goU2 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral"),
                    new Vector3(-350, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral")
                        .transform.rotation);
                goU2.transform.localScale = new Vector3(-10, 10, 1);
                
                
                Debug.Log("Combat " + battleI);
                battleI += 1;

                CameraController.inBattle = true;
                break;
            case 1: //G attaque D -------------------------------------------------------------------------
                battleCam.SetActive(true);
                if (goU1 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU1);
                }

                if (goU2 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU2);
                }

                U1 = u1;
                goU1 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Attack"),
                    new Vector3(-675, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Attack")
                        .transform.rotation);
                goU1.transform.localScale = new Vector3(10, 10, 1);

                if (battleInfo[0] > 0) {
                    U2 = u2;
                    goU2 = GameObject.Instantiate(
                        GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Damage"),
                        new Vector3(-325, 75, 300),
                        GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Damage")
                            .transform.rotation);
                    goU2.transform.localScale = new Vector3(-10, 10, 1);
                    if (battleInfo[0] == 2) {
                        //TODO etincelles
                    }
                }
                else {
                    //miss --------
                    U2 = u2;
                    goU2 = GameObject.Instantiate(
                        GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral"),
                        new Vector3(-300, 75, 300),
                        GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral")
                            .transform.rotation);
                    goU2.transform.localScale = new Vector3(-10, 10, 1);
                }

                battleI += 1;
                break;
            case 2: //Remise à 0 -------------------------------------------------------------------------
                if (goU1 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU1);
                }

                if (goU2 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU2);
                }

                U1 = u1;
                goU1 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral"),
                    new Vector3(-750, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral")
                        .transform.rotation);
                goU1.transform.localScale = new Vector3(10, 10, 1);

                U2 = u2;
                goU2 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral"),
                    new Vector3(-350, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral")
                        .transform.rotation);
                goU2.transform.localScale = new Vector3(-10, 10, 1);
                battleI += 1;


                Debug.Log("Combat " + battleI);
                if (battleInfo[1] <= 0) {
                    i = 4;
                }

                break;
            case 3: //Double attaque G sur D -------------------------------------------------------------------
                if (goU1 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU1);
                }

                if (goU2 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU2);
                }

                U1 = u1;
                goU1 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Attack"),
                    new Vector3(-675, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Attack")
                        .transform.rotation);
                goU1.transform.localScale = new Vector3(10, 10, 1);

                if (battleInfo[1] > 0) {
                    U2 = u2;
                    goU2 = GameObject.Instantiate(
                        GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Damage"),
                        new Vector3(-325, 75, 300),
                        GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Damage")
                            .transform.rotation);
                    goU2.transform.localScale = new Vector3(-10, 10, 1);
                    if (battleInfo[1] == 2) {
                        //TODO etincelles
                    }
                }
                else {
                    //miss ----------------
                    U2 = u2;
                    goU2 = GameObject.Instantiate(
                        GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral"),
                        new Vector3(-300, 75, 300),
                        GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral")
                            .transform.rotation);
                    goU2.transform.localScale = new Vector3(-10, 10, 1);
                }

                Debug.Log("Combat " + battleI);
                
                battleI += 1;
                break;
            case 4: // reset après double coup ---------------------------------------------------------------
                if (goU1 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU1);
                }

                if (goU2 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU2);
                }

                U1 = u1;
                goU1 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral"),
                    new Vector3(-750, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral")
                        .transform.rotation);
                goU1.transform.localScale = new Vector3(10, 10, 1);

                U2 = u2;
                goU2 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral"),
                    new Vector3(-350, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral")
                        .transform.rotation);
                goU2.transform.localScale = new Vector3(-10, 10, 1);
                
                Debug.Log("Combat " + battleI);
                battleI += 1;

                if (battleInfo[1] <= 0) {
                    i += 2;
                }
                if (battleInfo[4] == 1) {
                    i = -1;
                }

                break;
            case 5: // D attaque G -------------------------------------------------------------------------------
                if (goU1 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU1);
                }

                if (goU2 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU2);
                }

                U2 = u2;
                goU2 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Attack"),
                    new Vector3(-425, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Attack")
                        .transform.rotation);
                goU2.transform.localScale = new Vector3(-10, 10, 1);

                if (battleInfo[2] > 0) {
                    U1 = u1;
                    goU1 = GameObject.Instantiate(
                        GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Damage"),
                        new Vector3(-775, 75, 300),
                        GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Damage")
                            .transform.rotation);
                    goU1.transform.localScale = new Vector3(10, 10, 1);
                    if (battleInfo[2] == 2) {
                        //TODO etincelles
                    }
                }
                else {
                    // miss ----------------------------------
                    U2 = u2;
                    goU1 = GameObject.Instantiate(
                        GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral"),
                        new Vector3(-800, 75, 300),
                        GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral")
                            .transform.rotation);
                    goU1.transform.localScale = new Vector3(10, 10, 1);
                }
                
                battleI += 1;

                Debug.Log("Combat " + battleI);
                break;
            case 6: // remise à 0 ----------------------------------------------------------------------------------
                if (goU1 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU1);
                }

                if (goU2 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU2);
                }

                U1 = u1;
                goU1 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral"),
                    new Vector3(-750, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral")
                        .transform.rotation);
                goU1.transform.localScale = new Vector3(10, 10, 1);

                U2 = u2;
                goU2 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral"),
                    new Vector3(-350, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Neutral")
                        .transform.rotation);
                goU2.transform.localScale = new Vector3(-10, 10, 1);
                battleI += 1;
                if (battleInfo[3] <= 0) {
                    battleI = -1;
                }

                break;
            case 7: // double coup D sur G ---------------------------------------------------------------------------
                if (goU1 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU1);
                }

                if (goU2 != null) {
                    UnityEngine.GameObject.DestroyImmediate(goU2);
                }

                U2 = u2;
                goU2 = GameObject.Instantiate(
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Attack"),
                    new Vector3(-425, 75, 300),
                    GameObject.Find("Main/Battle/Sprites/" + u2.GetType().ToString().Split('+')[1] + "/Attack")
                        .transform.rotation);
                goU2.transform.localScale = new Vector3(-10, 10, 1);

                if (battleInfo[2] > 0) {
                    U1 = u1;
                    goU1 = GameObject.Instantiate(
                        GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Damage"),
                        new Vector3(-775, 75, 300),
                        GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Damage")
                            .transform.rotation);
                    goU1.transform.localScale = new Vector3(10, 10, 1);
                    if (battleInfo[2] == 2) {
                        //TODO etincelles
                    }
                }
                else {
                    // miss --------------------------------------
                    U1 = u1;
                    goU1 = GameObject.Instantiate(
                        GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral"),
                        new Vector3(-800, 75, 300),
                        GameObject.Find("Main/Battle/Sprites/" + u1.GetType().ToString().Split('+')[1] + "/Neutral")
                            .transform.rotation);
                    goU1.transform.localScale = new Vector3(10, 10, 1);
                }

                battleI += 1;
                break;
            default: // END ------------------------------------------------------------------------------------------
                GameObject.Destroy(goU1);
                GameObject.Destroy(goU2);
                battle = false;
                Battle_BG.SetActive(false);
                CameraController.ResetCameras();
                battleCam.SetActive(false);

                battleI = 0;

                CameraController.inBattle = false;
                break;
        }
    }
}