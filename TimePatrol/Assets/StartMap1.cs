﻿using System.Collections.Generic;
using UnityEngine;

public class StartMap1 : MonoBehaviour
{
    public static Game Start() {
        Maps m;
        if (Load.load) {
            m = Load.m;
            Load.load = false;
        }
        else {
            m = new Maps(Application.streamingAssetsPath + "/Levels/map_1.lvl", "now");
        }

        GameObject cursor = GameObject.Find("Main/Cursor/Cursor");
        GameObject walkable = GameObject.Find("Main/Cursor/Walkable");
        GameObject attackable = GameObject.Find("Main/Cursor/Attackable");
        GameObject moved = GameObject.Find("Main/Cursor/Moved");
        GameObject attacked = GameObject.Find("Main/Cursor/Attacked");
        GameObject mouseCollider = GameObject.Find("Main/Cursor/MouseCursor");

        GameObject texts = GameObject.Find("HUD/Txt_Panel");
        GameObject hud = GameObject.Find("HUD");
        Debug.Log("HUD: " + hud);
        hud.SetActive(false);

        Dictionary<GameObject, Unit> units = new Dictionary<GameObject, Unit>();

        GameObject blueKnight = GameObject.Find("/Main/Models/knight_blue");
        GameObject redKnight = GameObject.Find("/Main/Models/knight_red");
        GameObject blueMage = GameObject.Find("/Main/Models/mage_blue");
        GameObject redWizard = GameObject.Find("/Main/Models/mage_red");
        GameObject blueUrsari = GameObject.Find("/Main/Models/ursari_blue");
        GameObject redUrsari = GameObject.Find("/Main/Models/ursari_red");
        GameObject blueCrossBow = GameObject.Find("/Main/Models/crossbow_blue");
        GameObject redCrossBow = GameObject.Find("/Main/Models/crossbow_red");

        GameObject blueMecha = GameObject.Find("/Main/Models/mecha_blue");
        GameObject redMecha = GameObject.Find("/Main/Models/mecha_red");
        GameObject blueCyborg = GameObject.Find("/Main/Models/cyborg_blue");
        GameObject redCyborg = GameObject.Find("/Main/Models/cyborg_red");
        GameObject blueTank = GameObject.Find("/Main/Models/tank_blue");
        GameObject redTank = GameObject.Find("/Main/Models/tank_red");
        GameObject blueRoboSniper = GameObject.Find("/Main/Models/robo_blue");
        GameObject redRoboSniper = GameObject.Find("/Main/Models/robo_red");

        GameObject blueInfantry = GameObject.Find("/Main/Models/infantry_blue");
        GameObject redInfantry = GameObject.Find("/Main/Models/infantry_red");
        GameObject blueMedic = GameObject.Find("/Main/Models/medic_blue");
        GameObject redMedic = GameObject.Find("/Main/Models/medic_red");
        GameObject blueRiot = GameObject.Find("/Main/Models/riotman_blue");
        GameObject redRiot = GameObject.Find("/Main/Models/riotman_red");
        GameObject blueGunner = GameObject.Find("/Main/Models/gunner_blue");
        GameObject redGunner = GameObject.Find("/Main/Models/gunner_red");

        int id = 0;

        bool isPlayerOne = !Outils.isPlayerTwo;

        List<Unit> Team1 = isPlayerOne ? m.GetTeam1() : m.GetTeam2();
        List<Unit> Team2 = isPlayerOne ? m.GetTeam2() : m.GetTeam1();

        if (isPlayerOne)
        {
            foreach (Unit unit in Team1)
            {
                Vector3 vect = new Vector3();
                vect.x = unit.GetPos().x;
                vect.y = 1;
                vect.z = -unit.GetPos().y;

                if (unit is Melee.Knight)
                {
                    GameObject model = Instantiate(blueKnight, vect, blueKnight.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Range.CrossbowMan)
                {
                    GameObject model = Instantiate(blueCrossBow, vect, blueCrossBow.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Tank.Ursari)
                {
                    GameObject model = Instantiate(blueUrsari, vect, blueUrsari.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Special.Wizard)
                {
                    GameObject model = Instantiate(blueMage, vect, blueMage.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Melee.Infantry)
                {
                    GameObject model = Instantiate(blueInfantry, vect, blueInfantry.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Range.Gunner)
                {
                    GameObject model = Instantiate(blueGunner, vect, blueGunner.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Tank.AntiRiot)
                {
                    GameObject model = Instantiate(blueRiot, vect, blueRiot.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Special.Medic)
                {
                    GameObject model = Instantiate(blueMedic, vect, blueMedic.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Melee.Mecha)
                {
                    GameObject model = Instantiate(blueMecha, vect, blueMecha.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Range.RobotSniper)
                {
                    GameObject model = Instantiate(blueRoboSniper, vect, blueRoboSniper.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Tank.AssaultTank)
                {
                    GameObject model = Instantiate(blueTank, vect, blueTank.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Special.Cyborg)
                {
                    GameObject model = Instantiate(blueCyborg, vect, blueCyborg.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                unit.SetId(id);
                unit.SetPlayer(true);
                id++;
            }
        }

        foreach (Unit unit in Team2)
        {
            Vector3 vect = new Vector3();
            vect.x = unit.GetPos().x;
            vect.y = 1;
            vect.z = -unit.GetPos().y;

            if (unit is Melee.Knight)
            {
                GameObject model = Instantiate(redKnight, vect, redKnight.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Range.CrossbowMan)
            {
                GameObject model = Instantiate(redCrossBow, vect, redCrossBow.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Tank.Ursari)
            {
                GameObject model = Instantiate(redUrsari, vect, redUrsari.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Special.Wizard)
            {
                GameObject model = Instantiate(redWizard, vect, redWizard.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Melee.Infantry)
            {
                GameObject model = Instantiate(redInfantry, vect, redInfantry.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Range.Gunner)
            {
                GameObject model = Instantiate(redGunner, vect, redGunner.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Tank.AntiRiot)
            {
                GameObject model = Instantiate(redRiot, vect, redRiot.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Special.Medic)
            {
                GameObject model = Instantiate(redMedic, vect, redMedic.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Melee.Mecha)
            {
                GameObject model = Instantiate(redMecha, vect, redMecha.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Range.RobotSniper)
            {
                GameObject model = Instantiate(redRoboSniper, vect, redRoboSniper.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Tank.AssaultTank)
            {
                GameObject model = Instantiate(redTank, vect, redTank.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Special.Cyborg)
            {
                GameObject model = Instantiate(redCyborg, vect, redCyborg.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            unit.SetId(id);
            unit.SetPlayer(false);
            id++;
        }

        if (!isPlayerOne)
        {
            foreach (Unit unit in Team1)
            {
                Vector3 vect = new Vector3();
                vect.x = unit.GetPos().x;
                vect.y = 1;
                vect.z = -unit.GetPos().y;

                if (unit is Melee.Knight)
                {
                    GameObject model = Instantiate(blueKnight, vect, blueKnight.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Range.CrossbowMan)
                {
                    GameObject model = Instantiate(blueCrossBow, vect, blueCrossBow.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Tank.Ursari)
                {
                    GameObject model = Instantiate(blueUrsari, vect, blueUrsari.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Special.Wizard)
                {
                    GameObject model = Instantiate(blueMage, vect, blueMage.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Melee.Infantry)
                {
                    GameObject model = Instantiate(blueInfantry, vect, blueInfantry.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Range.Gunner)
                {
                    GameObject model = Instantiate(blueGunner, vect, blueGunner.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Tank.AntiRiot)
                {
                    GameObject model = Instantiate(blueRiot, vect, blueRiot.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Special.Medic)
                {
                    GameObject model = Instantiate(blueMedic, vect, blueMedic.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Melee.Mecha)
                {
                    GameObject model = Instantiate(blueMecha, vect, blueMecha.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Range.RobotSniper)
                {
                    GameObject model = Instantiate(blueRoboSniper, vect, blueRoboSniper.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Tank.AssaultTank)
                {
                    GameObject model = Instantiate(blueTank, vect, blueTank.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                if (unit is Special.Cyborg)
                {
                    GameObject model = Instantiate(blueCyborg, vect, blueCyborg.transform.rotation);
                    unit.SetModel(model);
                    units.Add(model, unit);
                }

                unit.SetId(id);
                unit.SetPlayer(true);
                id++;
            }
        }

        return new Game(m, units, cursor, walkable, attackable, texts, hud, isPlayerOne, Outils.isMultiplayer, moved,
            attacked,
            mouseCollider);
    }
}