﻿using System;
using Random = System.Random;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMap : MonoBehaviour, IPunObservable
{
    private Game g;
    public bool dialogue;
    public Dialogue dialogueText = new Dialogue();
    private Queue<string> sentences = new Queue<string>();
    public GameObject DialogScreen;
    public static Maps m;

    PhotonView photonView;
    //public GameObject SkipButton;


    // Start is called before the first frame update
    void Start()
    {
        
        Debug.Log("Starting " + SceneManager.GetActiveScene().name);
        
        photonView = PhotonView.Get(this);

        switch (SceneManager.GetActiveScene().name)
        {
            case "map0":
                g = StartMap0.Start();
                break;
            case "map1":
                g = StartMap1.Start();
                break;
            case "map2":
                g = StartMap2.Start();
                break;
            case "map3":
                g = StartMap3.Start();
                break;
            case "map4":
                g = StartMap4.Start();
                break;
            case "map5":
                g = StartMap5.Start();
                break;
            case "map6":
                g = StartMap6.Start();
                break;
            case "map7":
                g = StartMap7.Start();
                break;
            case "map8":
                g = StartMap8.Start();
                break;
            case "map9":
                g = StartMap9.Start();
                break;
            default:
                Debug.LogError("THIS SHOULD NOT HAPPEN: invalid scene name for map");
                throw new Exception("WHAT THE FUCKKK");
        }

        if (!Outils.isMultiplayer)
        {
            dialogue = true;
            
            //dialogue
            int lvl = SceneManager.GetActiveScene().buildIndex;

            DialogScreen.SetActive(true);

            string path = Application.streamingAssetsPath + "/Dialogs/Chapter" + (lvl - 4) + "_start.txt";

            dialogueText.SentenceParser(path);

            foreach (string sentence in dialogueText.sentences)
                sentences.Enqueue(sentence);

            FindObjectOfType<DialogueManager>().StartDialogue(sentences);
        }

        Debug.LogError("Starting game.\n" +
                       "Map: " + g.m.name + ", " +
                       "Multiplayer: " + Outils.isMultiplayer + ", " +
                       "IsPlayerTwo: " + Outils.isPlayerTwo);
    }

    // Update is called once per frame
    void Update()
    {
        if (dialogue && (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) /*SkipButton*/)
        {
            dialogue = false;
            DialogScreen.SetActive(false);
        }

        if (!dialogue || Outils.isMultiplayer)
        {
            g.Update();

            if (g.IsMultiplayer())
            {
                if (!Outils.isPlayerTwo && !Outils.sentSeed)
                {
                    photonView.RPC("SendSeed", RpcTarget.Others, Outils.seed);
                    Outils.sentSeed = true;
                }
                
                if (g.RPCDefender != null && g.RPCAttacker != null)
                {
                    photonView.RPC("Attack", RpcTarget.Others,
                        g.RPCAttacker.GetId(), g.RPCDefender.GetId());
                }
                else if (g.RPCMovePos.x != -1 && g.RPCMovePos.y != -1 && g.RPCAttacker != null)
                {
                    photonView.RPC("Move", RpcTarget.Others,
                        g.RPCAttacker.GetId(), g.RPCMovePos.x, g.RPCMovePos.y);
                }
                else if (g.RPCSwitchTurn)
                {
                    photonView.RPC("SwitchTurn", RpcTarget.Others);
                }
            }
        }
    }

    [PunRPC]
    void Move(int unitID, int x, int y)
    {
        g.RPCMove(unitID, x, y);
        Debug.Log("RPC: Moving unit " + unitID + " to (" + x + "," + y + ")");
    }

    [PunRPC]
    void Attack(int attackerID, int defenderID)
    {
        g.RPCAttack(attackerID, defenderID);
        Debug.Log("RPC: Unit " + attackerID + " is attacking unit " + defenderID);
    }

    [PunRPC]
    void SwitchTurn()
    {
        g.SwitchTurn();
        Debug.Log("RPC: Switching turns");
    }

    [PunRPC]
    void SendSeed(int seed)
    {
        Outils.seed = seed;
        Debug.Log("RPC: Seed set to " + seed);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        photonView = PhotonView.Get(this);
    }

    public Game getGame()
    {
        return g;
    }

    public void setGame(Game g)
    {
        this.g = g;
    }
}