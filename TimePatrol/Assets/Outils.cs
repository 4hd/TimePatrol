using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public static class Outils {
    public static bool isMultiplayer { get; set; }
    
    public static bool isPlayerTwo { get; set; }
    
    public static int seed = new Random().Next(Int32.MaxValue);

    public static bool sentSeed { get; set; }
    
    public static string[] FileToString(string filePath) {
        return System.IO.File.ReadAllLines(@filePath);
    }

    public static void StringToFile(string[] str, string fileName) {
        System.IO.File.WriteAllLines(@fileName, str);
    }

    public static string[] IntArrayToString(int[,] Array) {
        string[] res = new string[Array.GetLength(1)];
        for (int y = 0; y < Array.GetLength(1); y++) {
            for (int x = 0; x < Array.GetLength(0); x++) {
                res[y] = res[y] + ' ' + Array[x, y] + "";
            }
        }

        return res;
    }

    public static string[] StringListToArray(List<string> s) {
        int length = s.Count;
        string[] res = new string[length];

        for (int i = 0; i < length; i++) {
            res[i] = s[i];
        }

        return res;
    }

    public static int[,] StringToIntArray(string[] str) {
        int maxLen = 0;
        int strLen = str.Length;

        //transforme le texte en Liste de Int sur les X et en Tableau de Liste sur les Y
        List<List<int>> intTmp = new List<List<int>>();
        int n = 0;
        int tmp;

        for (int y = 0; y < strLen; y++) {
            intTmp.Add(new List<int>());
            int max = 0;
            for (int x = 0; x < str[y].Length; x++) {
                if (str[y][x] == ' ') {
                    intTmp[y].Add(n);
                    n = 0;
                }
                else {
                    if (int.TryParse(str[y][x] + "", out tmp)) {
                        n *= 10;
                        n += tmp;
                        max += 1;
                    }
                }
            }

            intTmp[y].Add(n);
            n = 0;
            if (max > maxLen) {
                maxLen = max;
            }
        }

        //transforme le tableau de liste precedent en tableau a 2D
        int[,] res = new int[maxLen, intTmp.Count]; //res[x,y]
        for (int y = 0; y < intTmp.Count; y++) {
            //met toutes les valeurs à -1
            for (int x = 0; x < intTmp[y].Count; x++) {
                res[x, y] = -1;
            }
        }

        for (int y = 0; y < intTmp.Count; y++) {
            for (int x = 0; x < intTmp[y].Count; x++) {
                res[x, y] = intTmp[y][x];
            }
        }

        return res;
    }
    
    public static void Concatenate(List<Tile> liste1, List<Tile> liste2) {
        //concatene 2 listes et les met dans liste1
        foreach (var e2 in liste2) {
            if (!liste1.Contains(e2)) {
                liste1.Add(e2);
            }
        }
    }

    public static float Distance(Vector2 v1, Vector2 v2)
    {
        return (float) Math.Sqrt(Math.Pow(v2.x - v1.x, 2) + Math.Pow(v2.y - v1.y, 2));
    }

    public static Unit GetNearest(Unit self, List<Unit> units)
    {
        Unit nearest = null;

        foreach (var u in units)
        {
            if (!u.IsPlayer()) continue;
            
            if (nearest == null)
            {
                nearest = u;
            }
            else if (Distance(self.GetPos(), u.GetPos()) < Distance(self.GetPos(), nearest.GetPos()))
            {
                nearest = u;
            }
        }

        return nearest;
    }
}