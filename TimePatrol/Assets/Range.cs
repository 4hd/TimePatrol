public class Range : Unit {
    public class CrossbowMan : Range {
        public CrossbowMan() {
            health = 60;
            attack = 50;
            defense = 15;
            mobility = 3;
            attack_range = 2;
            speed = 70;
            dodge = 20;
            luck = 35;
        }
    }

    public class Gunner : Range {
        public Gunner() {
            health = 55;
            attack = 45;
            defense = 17;
            mobility = 3;
            attack_range = 2;
            speed = 92;
            dodge = 20;
            luck = 20;
        }
    }

    public class RobotSniper : Range {
        public RobotSniper() {
            health = 50;
            attack = 40;
            defense = 15;
            mobility = 4;
            attack_range = 3; //or 4 
            speed = 110;
            dodge = 35;
            luck = 30;
        }
    }
}