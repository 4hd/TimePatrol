﻿using System.Collections.Generic;
using UnityEngine;

public class StartTestMap : MonoBehaviour {
    private Maps m;
    private GameObject blueKnight;
    private GameObject redKnight;
    private GameObject blueMage;
    private GameObject redWizard;
    private GameObject blueUrsari;
    private GameObject redUrsari;
    private GameObject blueCrossBow;
    private GameObject redCrossBow;
    private Dictionary<GameObject, Unit> units;
    private Unit selectedUnit = null;

    // Start is called before the first frame update
    void Start() {
        m = new Maps("Assets/Levels/Map Test.lvl");
        units = new Dictionary<GameObject, Unit>();
        
        blueKnight = GameObject.Find("/Main/knight_blue");
        redKnight = GameObject.Find("/Main/knight_red");
        blueMage = GameObject.Find("/Main/mage_blue");
        redWizard = GameObject.Find("/Main/mage_red");
        blueUrsari = GameObject.Find("/Main/ursari_blue");
        redUrsari = GameObject.Find("/Main/ursari_red");
        blueCrossBow = GameObject.Find("/Main/crossbow_blue");
        redCrossBow = GameObject.Find("/Main/crossbow_red");

        foreach (Unit unit in m.GetTeam1()) {
            Vector3 vect = new Vector3();
            vect.x = unit.GetPos().x;
            vect.y = 1;
            vect.z = -unit.GetPos().y;

            if (unit is Melee.Knight) {
                GameObject model = Instantiate(blueKnight, vect, blueKnight.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Range.CrossbowMan) {
                GameObject model = Instantiate(blueCrossBow, vect, blueCrossBow.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Tank.Ursari) {
                GameObject model = Instantiate(blueUrsari, vect, blueUrsari.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Special.Wizard) {
                GameObject model = Instantiate(blueMage, vect, blueMage.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }
        }

        foreach (Unit unit in m.GetTeam2()) {
            Debug.Log(unit == null);
            Vector3 vect = new Vector3();
            vect.x = unit.GetPos().x;
            vect.y = 1;
            vect.z = -unit.GetPos().y;

            if (unit is Melee.Knight) {
                GameObject model = Instantiate(redKnight, vect, redKnight.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Range.CrossbowMan) {
                GameObject model = Instantiate(redCrossBow, vect, redCrossBow.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Tank.Ursari) {
                GameObject model = Instantiate(redUrsari, vect, redUrsari.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }

            if (unit is Special.Wizard) {
                GameObject model = Instantiate(redWizard, vect, redWizard.transform.rotation);
                unit.SetModel(model);
                units.Add(model, unit);
            }
        }
    }

    // Update is called once per frame
    void Update() {
        //recupere l'unite selectionnee
        if (Input.GetMouseButtonDown(0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 1000)) {
                Debug.Log(hit.transform.gameObject.name);
                if (units.ContainsKey(hit.transform.gameObject)) {
                    selectedUnit = units[hit.transform.gameObject];
                    Debug.Log("HP: " + selectedUnit.GetHealth());
                    Debug.Log("Attack: " + selectedUnit.GetAttack());
                    Debug.Log("Defense: " + selectedUnit.GetDefense());
                    Debug.Log("Mobility: "+selectedUnit.GetMobility());
                    Debug.Log("Speed: " + selectedUnit.GetSpeed());
                    Debug.Log("Dodge: " + selectedUnit.GetDodge());
                }
                else {
                    selectedUnit = null;
                }
            }
        }
        
        
    }
}