using Random = System.Random;

/* 1: Récupérer l'unité qui lance la bataille (atker), et celle qui la subit (defer) 
 * 2: Changer la caméra du plateau vers la caméra de bataille
 * 3: Afficher à gauche le sprite neutral de l'atker et à droite en inversé celui du defer
 * 4: Changer le sprite de l'atker vers son sprite d'attack
 * 5: Appeler Combat (calculer ses dégats etc...)
 * 6: Changer le sprite du defer vers son sprite de prise de dégats
 * 7: Changer le sprite de l'atker vers son sprite neutral
 * 8: Changer le sprite du defer vers son sprite neutral
 * 9: refaire une boucle à partir de (4) mais en inversant atker et defer
 * 10: revenir sur la caméra de plateau 
 */
/*atker = GameObject.Find();
defer = GameObject.Find();
var cam1 : Camera;
var battlecam : Camera;
function Start() {
cam1.enabled = true;
cam2.enabled = false;
}


Place*/
public class Battle : Unit
{
    public static int Calc_DMG(Unit atker, Unit defer, bool crit, Maps m) {
        int ATK; //Rider ne trouvait pas ATK sans ça
        if (crit) {
            ATK = atker.GetAttack() * 2;
        }
        else {
            ATK = atker.GetAttack();
        }

        float DEF = defer.GetDefense();
        switch (m.FindTile(defer.GetPos()).GetMobility()) {
            case 2:
                DEF *= 1.3f;
                break;
            case 5:
                DEF *= 1.5f;
                break;
        }

        if (defer is Special.Medic && atker.IsPlayer() == defer.IsPlayer()) {
            return 0;
        }

        if (atker is Special.Medic && atker.IsPlayer() == defer.IsPlayer()) {
            return -(atker.GetHeal());
        }
        return (int) (ATK - DEF);
    }

// appeler RemoveHealth avec en attribut la valeur de Calc_DMG
    public static bool Double_hit(Unit atker, Unit defer, Maps m) {
        float atkSpd = atker.GetSpeed();
        float defSpd = 1.5f * defer.GetSpeed();

        switch (m.FindTile(atker.GetPos()).GetMobility()) {
            case 3:
                atkSpd *= 0.8f;
                break;
            case 5:
                atkSpd *= 0.7f;
                break;
        }
        
        switch (m.FindTile(defer.GetPos()).GetMobility()) {
            case 3:
                atkSpd *= 0.8f;
                break;
            case 5:
                atkSpd *= 0.7f;
                break;
        }
        
        return atkSpd >= defSpd;
    }

    public static bool Dodge(Unit defer, Maps m) {
        float dodge = defer.GetDodge();
        if (m.FindTile(defer.GetPos()).GetMobility() == 3) {
            dodge *= 0.7f;
        }
        
        int hit_prob = new Random(Outils.seed).Next(0, 100);
        return (hit_prob <= dodge);
    }

    public static bool Crit(Unit atker) {
        int crit = atker.GetLuck();
        
        int crit_prob = new Random(Outils.seed).Next(0, 100);
        return (crit_prob <= crit);
    }

    public static bool Combat(Unit atker, Unit defer, bool crit, Maps m) {
        if (!Dodge(defer, m)) {
            defer.RemoveHealth(Calc_DMG(atker, defer, crit, m));
            return true;
        }
        return false;
    }
}