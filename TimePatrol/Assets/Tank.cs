public class Tank : Unit {
    public class Ursari : Tank {
        public Ursari() {
            health = 130;
            attack = 40;
            defense = 35;
            mobility = 3;
            attack_range = 1;
            speed = 60;
            dodge = 5;
            luck = 10;
        }
    }

    public class AntiRiot : Tank {
        public AntiRiot() {
            health = 115;
            attack = 35;
            defense = 27;
            mobility = 4;
            attack_range = 1;
            speed = 60;
            dodge = 10;
            luck = 10;
        }
    }

    public class AssaultTank : Tank {
        public AssaultTank() {
            health = 100;
            attack = 40;
            defense = 22;
            mobility = 5;
            attack_range = 1; //or 2
            speed = 80;
            dodge = 20;
            luck = 20;
        }
    }
}