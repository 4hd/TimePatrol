public class Melee : Unit {
    public class Knight : Melee {
        public Knight() {
            health = 100;
            attack = 70;
            defense = 25;
            mobility = 4;
            attack_range = 1;
            speed = 70;
            dodge = 10;
            luck = 10;
        }
    }

    public class Infantry : Melee {
        public Infantry() {
            health = 80;
            attack = 55;
            defense = 22;
            mobility = 5;
            attack_range = 1;
            speed = 92;
            dodge = 15;
            luck = 15;
        }
    }

    public class Mecha : Melee {
        public Mecha() {
            health = 70;
            attack = 40;
            defense = 22;
            mobility = 6;
            attack_range = 1;
            speed = 140;
            dodge = 20;
            luck = 30;
        }
    }
}