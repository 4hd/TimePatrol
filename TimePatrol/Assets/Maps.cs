using System.Collections.Generic;
using UnityEngine;


public class Maps {
    Dictionary<Vector2Int, Tile> tileDict;
    List<Tile> tileList;
    List<Unit> unitList1;
    List<Unit> unitList2;
    private string race1;
    private string race2;
    int width;
    int heigth;
    public string name;

    public Maps(string name, string race1 = "past", string race2 = "past") {
        string[] tmp = name.Split('/');
        this.name = tmp[tmp.Length - 1];
        this.name = this.name.Split('.')[0];
        
        int[,] mapData = OpenMap(name);

        width = mapData.GetLength(0);
        heigth = mapData.GetLength(1);

        tileDict = new Dictionary<Vector2Int, Tile>();
        tileList = new List<Tile>();

        CreateTiles(mapData);

        this.race1 = race1;
        this.race2 = race2;

        unitList1 = new List<Unit>();
        unitList2 = new List<Unit>();

        SpawnUnits(name, race1, race2);
    }
    
    public Maps(string name, List<Unit> team1, List<Unit> team2) {
        string[] tmp = name.Split('/');
        this.name = tmp[tmp.Length - 1];
        this.name = this.name.Split('.')[0];
        
        int[,] mapData = OpenMap(name);

        width = mapData.GetLength(0);
        heigth = mapData.GetLength(1);

        tileDict = new Dictionary<Vector2Int, Tile>();
        tileList = new List<Tile>();

        CreateTiles(mapData);

        unitList1 = team1;
        unitList2 = team2;

        SpawnUnits(name, race1, race2);
    }

    public int[,] OpenMap(string mapName) {
        string[] tmp = Outils.FileToString(mapName);

        List<string> mapList = new List<string>(); //cree la partie map
        int i = 0;
        while (i < tmp.Length && tmp[i] != "@") {
            mapList.Add(tmp[i]);
            i++;
        }

        return Outils.StringToIntArray(Outils.StringListToArray(mapList));
    }

    //cree chaque case et les met dans la liste tileList et le dictionnaire tileDict
    public void CreateTiles(int[,] mapData)
    {
        for (int y = 0; y < mapData.GetLength(1); y++) {
            for (int x = 0; x < mapData.GetLength(0); x++) {
                Tile t = new Tile(mapData[x, y], x, y);
                tileList.Add(t);
                tileDict.Add(t.GetPos(), t);
            }
        }

        foreach (KeyValuePair<Vector2Int, Tile> e in tileDict
        ) //cherche les cases adjacentes de chaque cases et les ajoutent a Tile.adjacentTiles
        {
            Tile t = e.Value;
            Tile u;

            Vector2Int up = new Vector2Int(); //à suppr?
            up.x = e.Key.x;
            up.y = e.Key.y + 1;
            u = FindTile(up);
            if (u != null) {
                t.adjacentTiles.Add(u);
            }

            Vector2Int down = new Vector2Int();
            down.x = e.Key.x;
            down.y = e.Key.y - 1;
            u = FindTile(down);
            if (u != null) {
                t.adjacentTiles.Add(u);
            }

            Vector2Int left = new Vector2Int();
            left.x = e.Key.x - 1;
            left.y = e.Key.y;
            u = FindTile(left);
            if (u != null) {
                t.adjacentTiles.Add(u);
            }

            Vector2Int right = new Vector2Int();
            right.x = e.Key.x + 1;
            right.y = e.Key.y;
            u = FindTile(right);
            if (u != null) {
                t.adjacentTiles.Add(u);
            }
        }
    }

    public void SpawnUnits(string mapName, string team1, string team2) {
        string[] tmp = Outils.FileToString(mapName);

        int i = 0;
        //permet de passer la partie cases
        while (i < tmp.Length && tmp[i] != "@") {
            i++;
        }

        i++;

        //recupere les donnees de spawn de l'equipe 1 (n0)
        while (i < tmp.Length && tmp[i] != "#") {
            int j = 2;
            int x = 0;
            int y = 0;
            //recupere x
            while (j < tmp[i].Length && tmp[i][j] != ' ') {
                int tmpX;
                if (int.TryParse(tmp[i][j] + "", out tmpX)) {
                    x *= 10;
                    x += tmpX;
                }

                j++;
            }

            j++;
            //recupere y
            while (j < tmp[i].Length && tmp[i][j] != ' ') {
                int tmpY;
                if (int.TryParse(tmp[i][j] + "", out tmpY)) {
                    y *= 10;
                    y += tmpY;
                }

                j++;
            }


            Unit u;
            //cree unite et la met dans unitList
            switch (team1) {
                case "past":
                    switch (tmp[i][0]) {
                        case 'M':
                            u = new Melee.Knight();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                        case 'R':
                            u = new Range.CrossbowMan();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                        case 'T':
                            u = new Tank.Ursari();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                        case 'S':
                            u = new Special.Wizard();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                    }

                    break;
                case "now":
                    switch (tmp[i][0]) {
                        case 'M':
                            u = new Melee.Infantry();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                        case 'R':
                            u = new Range.Gunner();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                        case 'T':
                            u = new Tank.AntiRiot();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                        case 'S':
                            u = new Special.Medic();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                    }

                    break;
                case "future":
                    switch (tmp[i][0]) {
                        case 'M':
                            u = new Melee.Mecha();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                        case 'R':
                            u = new Range.RobotSniper();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                        case 'T':
                            u = new Tank.AssaultTank();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                        case 'S':
                            u = new Special.Cyborg();
                            u.SetPos(x, y);
                            unitList1.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("blue");
                            break;
                    }

                    break;
            }
            

            i++;
        }


        //recupere les donnees de spawn de l'equipe 2
        while (i < tmp.Length) {
            int j = 2;
            int x = 0;
            int y = 0;
            //recupere x
            while (j < tmp[i].Length && tmp[i][j] != ' ') {
                int tmpX;
                if (int.TryParse(tmp[i][j] + "", out tmpX)) {
                    x *= 10;
                    x += tmpX;
                }

                j++;
            }

            j++;
            //recupere y
            while (j < tmp[i].Length && tmp[i][j] != ' ') {
                int tmpY;
                if (int.TryParse(tmp[i][j] + "", out tmpY)) {
                    y *= 10;
                    y += tmpY;
                }

                j++;
            }

            Unit u;
            //cree unite et la met dans unitList
            switch (team2) {
                case "past":
                    switch (tmp[i][0]) {
                        case 'M':
                            u = new Melee.Knight();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                        case 'R':
                            u = new Range.CrossbowMan();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                        case 'T':
                            u = new Tank.Ursari();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                        case 'S':
                            u = new Special.Wizard();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                    }

                    break;
                case "now":
                    switch (tmp[i][0]) {
                        case 'M':
                            u = new Melee.Infantry();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                        case 'R':
                            u = new Range.Gunner();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                        case 'T':
                            u = new Tank.AntiRiot();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                        case 'S':
                            u = new Special.Medic();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                    }

                    break;
                case "future":
                    switch (tmp[i][0]) {
                        case 'M':
                            u = new Melee.Mecha();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                        case 'R':
                            u = new Range.RobotSniper();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                        case 'T':
                            u = new Tank.AssaultTank();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                        case 'S':
                            u = new Special.Cyborg();
                            u.SetPos(x, y);
                            unitList2.Add(u);
                            foreach (Tile t in tileList) {
                                if (t.GetPos().x == x && t.GetPos().y == y) {
                                    t.SetUnit(u);
                                }
                            }

                            u.SetTeam("red");
                            break;
                    }

                    break;
            }

            i++;
        }
    }

    public List<Tile> Movement(Unit u) //renvoie une liste ou le joueur peut se deplacer s'il est sur la carte
    {
        Vector2Int pos = u.GetPos();

        Tile t = FindTile(pos);
        
        if (t != null) {
            List<Tile> tmp = Walkable(t, u.GetMobility() + 1);
            List<Tile> res = new List<Tile>();
            foreach (Tile tile in tmp) {
                if (!IsOccupied(tile)) {
                    res.Add(tile);
                }
            }

            res.Add(t);
            
            tmp = new List<Tile>();
            foreach (Tile tile in t.GetAdjacent()) {
                if (tile.GetMobility() != 0 && tile.GetUnit() == null) {
                    tmp.Add(tile);
                }
            }
            Outils.Concatenate(res, tmp);

            return res;
        }
        else {
            return null;
        }
    }

    private List<Tile> Walkable(Tile curr, int mobi) {
        //revoie la liste des cases avec deplacement valide
        if (mobi > 0) {
            List<Tile> tiles = new List<Tile>();
            foreach (var e in curr.GetAdjacent()) {
                if (e.GetMobility() > 0) {
                    if (e.GetMobility() <= mobi) {
                        tiles.Add(e);
                    }
                }
            }

            List<Tile> res = new List<Tile>();
            res.Add(curr);
            foreach (var e in tiles) {
                Outils.Concatenate(res, Walkable(e, mobi - e.GetMobility()));
            }

            return res;
        }
        else {
            return new List<Tile>();
        }
    }

    // attack range tiles
    public List<Tile> Range(Unit u) {
        List<Tile> res = _Range(FindTile(u.GetPos()), u.GetAttack_range() + 1);
        res.Remove(FindTile(u.GetPos()));
        return res;
    }

    public List<Tile> _Range(Tile curr, int range) {
        if (range > 0) {
            List<Tile> tiles = new List<Tile>();
            foreach (var e in curr.GetAdjacent()) {
                tiles.Add(e);
            }

            List<Tile> res = new List<Tile>();
            res.Add(curr);
            foreach (var e in tiles) {
                Outils.Concatenate(res, _Range(e, range - 1));
            }

            return res;
        }
        else {
            return new List<Tile>();
        }
    }

    private bool IsOccupied(Tile t) {
        return t.GetUnit() != null;
    }

    public bool ChangeUnitPos(Unit u, Tile t) {
        if (tileList.Contains(t) && Movement(u).Contains(t) && (t.GetUnit() == null || t.GetUnit() == u)) {
            FindTile(u.GetPos()).SetUnit(null);
            u.SetPosVect(t.GetPos());
            t.SetUnit(u);
            u.GetModel().transform.position = new Vector3(t.GetPos().x, 1f, -t.GetPos().y);
            return true;
        }

        return false;
    }

    public Tile FindTile(Vector2Int v) {
        foreach (Tile t in tileList) {
            if (v.x == t.GetPos().x && v.y == t.GetPos().y) {
                return t;
            }
        }

        return null;
    }

    public int GetWidth() {
        return width;
    }

    public int GetHeight() {
        return heigth;
    }

    public string GetRace1() {
        return race1;
    }

    public string GetRace2() {
        return race2;
    }

    public List<Tile> GetTileList() {
        return tileList;
    }

    public List<Unit> GetTeam1() {
        return unitList1;
    }

    public List<Unit> GetTeam2() {
        return unitList2;
    }

    public void SetTeam1(List<Unit> l) {
        unitList1 = l;
    }

    public void SetTeam2(List<Unit> l) {
        unitList2 = l;
    }
}