using UnityEngine;

public abstract class Unit {
    protected string name;
    protected int id;
    protected int health;
    protected int attack;
    protected int defense;
    protected int mobility;
    protected int attack_range;
    protected int detect_range = 8;
    protected int heal;
    protected int speed;
    protected int dodge;
    protected int luck;
    protected string team;
    protected Vector2Int pos;
    protected GameObject model;
    protected GameObject sprite;
    public GameObject effect;
    
    // false is AI
    protected bool isPlayer = true;
    
    public bool IsDetected(Unit u)
    {
        return Outils.Distance(pos, u.GetPos()) <= detect_range;
    }

    public int GetId() {
        return id;
    }

    public void SetId(int id) {
        this.id = id;
    }
    
    public bool IsPlayer()
    {
        return isPlayer;
    }

    public void SetPlayer(bool b)
    {
        isPlayer = b;
    }

    public void SetModel(GameObject model) {
        this.model = model;
    }

    public GameObject GetModel() {
        return model;
    }

    public void SetTeam(string team) {
        this.team = team;
    }

    public string GetTeam() {
        return team;
    }

    public void SetPos(int x, int y) {
        this.pos = new Vector2Int(x, y);
    }

    public void SetPosVect(Vector2Int pos) {
        this.pos = pos;
    }

    public Vector2Int GetPos() {
        return pos;
    }

    public int GetMobility() {
        return mobility;
    }

    public int GetHealth() {
        return health;
    }

    public void SetHealth(int h) {
        health = h;
    }

    public int GetAttack() {
        return attack;
    }

    public int GetDefense() {
        return defense;
    }

    public int GetAttack_range() {
        return attack_range;
    }

    public int GetSpeed() {
        return speed;
    }

    public int GetDodge() {
        return dodge;
    }

    public int GetLuck() {
        return luck;
    }

    public int GetHeal() {
        return heal;
    }

    public int GetDetect_range() {
        return detect_range;
    }

    public void RemoveHealth(int health) {
        this.health -= health;
    }
}