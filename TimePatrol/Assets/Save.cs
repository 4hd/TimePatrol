using System.Collections.Generic;

public class Save {
    public static void GameSaver(Game g) {
        List<string> res = new List<string>();

        res.Add(g.m.name);

        res.Add("@"+g.m.GetRace1());
        foreach (Unit u in g.m.GetTeam1()) {
            string tmp = "";
            tmp += ("" + u.GetType()).Split('+')[1] + ':';
            tmp += u.GetHealth().ToString() + ':';
            tmp += u.GetPos().x.ToString() + ':' + u.GetPos().y.ToString();

            res.Add(tmp);
        }

        res.Add("#"+g.m.GetRace2());
        foreach (Unit u in g.m.GetTeam2()) {
            string tmp = "";
            tmp += ("" + u.GetType()).Split('+')[1] + ':';
            tmp += u.GetHealth().ToString() + ':';
            tmp += u.GetPos().x.ToString() + ':' + u.GetPos().y.ToString();

            res.Add(tmp);
        }

        Outils.StringToFile(Outils.StringListToArray(res), "Game Save.sav");
    }

    public static void LevelSaver(int n) {
        Outils.StringToFile(new[] {"" + n}, "Game Level.sav");
    }

    public static void ResetSavedGame() {
        Outils.StringToFile(new[] {""}, "Game Save.sav");
    }
}