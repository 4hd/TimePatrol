﻿using UnityEngine;

public class EndGame : MonoBehaviour
{
    public GameObject Vic_Screen;

    public GameObject Def_Screen;

    public static bool End_Screen = false;

    public static bool defeat = false;

    public static bool victory = false;

//  peter do do do do do do do

    void Update()
    {
        if (victory || Input.GetKeyDown(KeyCode.W) && Input.GetKeyDown(KeyCode.I) && Input.GetKeyDown(KeyCode.N))
        {
            Victory();
        }

        if (defeat || Input.GetKeyDown(KeyCode.D) && Input.GetKeyDown(KeyCode.E) && Input.GetKeyDown(KeyCode.F))
        {
            Defeat();
        }
        
        victory = false;
        defeat = false;
    }

    public void Victory()
    {
        Vic_Screen.SetActive(true);
        PauseMenuGoodOne.gameIsPaused = true;
        End_Screen = true;
        victory = false;
    }

    public void Defeat()
    {
        Def_Screen.SetActive(true);
        PauseMenuGoodOne.gameIsPaused = true;
        End_Screen = true;
        defeat = false;
    }
    /*
    public void Quit() {
        Vic_Screen.SetActive(false);
        Def_Screen.SetActive(false);
        PauseMenuGoodOne.gameIsPaused = false;
        End_Screen = false;
        LoadSceneOnClick.LoadByIndex(0);
    }
    */

    public void Quit_Vic()
    {
        Vic_Screen.SetActive(false);
        PauseMenuGoodOne.gameIsPaused = false;
        End_Screen = false;
        LoadSceneOnClick.LoadByIndex(0);
    }

    public void Quit_Def()
    {
        Def_Screen.SetActive(false);
        PauseMenuGoodOne.gameIsPaused = false;
        End_Screen = false;
        LoadSceneOnClick.LoadByIndex(0);
    }
}