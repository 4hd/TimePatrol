﻿using UnityEngine;
using UnityEngine.SceneManagement;

using Photon.Pun;
using Photon.Realtime;


namespace Com.MyCompany.MyGame
{
    public class GameManager : MonoBehaviourPunCallbacks
    {

        #region Photon Callbacks

        /// <summary>
        /// Called when the local player left the room. We need to load the launcher scene.
        /// </summary>
        public override void OnLeftRoom()
        {
            Outils.isMultiplayer = false;
            SceneManager.LoadScene(0);
        }
        
        public override void OnPlayerEnteredRoom(Player other)
        {
            Outils.isMultiplayer = true;
            Debug.LogFormat("OnPlayerEnteredRoom() {0}", other.NickName); // not seen if you're the player connecting
            Debug.Log("MasterClient is " + PhotonNetwork.IsMasterClient);

            if (PhotonNetwork.IsMasterClient)
            {
//                Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom

                Outils.isPlayerTwo = false;

                LoadArena();
            }
            else
            {
                Outils.isPlayerTwo = true;
            }
        }


        public override void OnPlayerLeftRoom(Player other)
        {
            Debug.LogFormat("OnPlayerLeftRoom() {0}", other.NickName); // seen when other disconnects


            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("OnPlayerLeftRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom

                LoadArena();
            }
            Outils.isMultiplayer = false;
        }

        #endregion
        
        #region Private Methods


        void LoadArena()
        {
            Debug.Log("MasterClient? " + PhotonNetwork.IsMasterClient);
            
            Outils.isMultiplayer = true;
            
            if (!PhotonNetwork.IsMasterClient)
            {
                Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
                Outils.isPlayerTwo = true;
            }
            else
            {
                Outils.isPlayerTwo = false;
            }

            Debug.LogFormat("PhotonNetwork : Loading Level : {0}", PhotonNetwork.CurrentRoom.PlayerCount);

            PhotonNetwork.LoadLevel("map1");
        }

        #endregion


        #region Public Methods


        public void LeaveRoom()
        {
            Debug.Log("Bye bitch");
            
            PhotonNetwork.LeaveRoom();

            Outils.isMultiplayer = false;
        }


        #endregion
    }
}