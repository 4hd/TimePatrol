﻿using UnityEngine;


[System.Serializable] // pour que la classe apparaisse dans l'inspecteur
public class Sound
{
    public string name;
    
    public AudioClip clip;

    [Range(0f, 1f)]    //pour qu'on ait des sliders
    public float volume;
    [Range(.1f,3)]
    public float pitch;

    public bool loop;

    [HideInInspector]
    public AudioSource source;
}
