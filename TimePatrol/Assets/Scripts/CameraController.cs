﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static GameObject[] cameras;
    public static int currentCameraIndex;
    public GameObject Canvas;
    public static bool inBattle = false;

    // initialisation 
    void Start()
    {
        cameras = new GameObject[3];
        cameras[0] = GameObject.Find("Cameras/CamRotatePoint/Top_Cam");
        cameras[1] = GameObject.Find("Cameras/CamRotatePoint/Panoramic_Cam");
        cameras[2] = GameObject.Find("Cameras/CamRotatePoint/View_Cam");

        currentCameraIndex = 1;

        //Turn all cameras off, except the first default one
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].gameObject.SetActive(false);
        }

        //If any cameras were added to the controller, enable the first one
        if (cameras.Length > 0)
        {
            cameras[0].gameObject.SetActive(true);
            Debug.Log("Camera with name: " + cameras[0].GetComponent<Camera>().name + ", is now enabled");
        }
    }

    // Update is called once per frame
    void Update()
    {
        //switch to the next cam when c button pressed
        //Set the camera at the current index to inactive, and set the next one in the array to active
        //When we reach the end of the camera array, move back to the beginning or the array.
        if (Game.PositiveEdge("camera") && !inBattle && !PauseMenuGoodOne.gameIsPaused)
        //if (Input.GetKeyDown(KeyCode.C) && !inBattle)
        {
            currentCameraIndex++;
            Debug.Log("C button has been pressed. Switching to the next camera");
            if (currentCameraIndex < cameras.Length)
            {
                foreach (GameObject camera in cameras) {
                    camera.SetActive(false);
                }
                cameras[currentCameraIndex].gameObject.SetActive(true);
                Canvas.GetComponent<Canvas>().worldCamera =
                    cameras[currentCameraIndex].gameObject.GetComponent<Camera>();
                Debug.Log("Camera with name: " + cameras[currentCameraIndex].GetComponent<Camera>().name +
                          ", is now enabled");
            }
            else
            {
                foreach (GameObject camera in cameras) {
                    camera.SetActive(false);
                }
                currentCameraIndex = 0;
                cameras[currentCameraIndex].gameObject.SetActive(true);
                Canvas.GetComponent<Canvas>().worldCamera =
                    cameras[currentCameraIndex].gameObject.GetComponent<Camera>();
                Debug.Log("Camera with name: " + cameras[currentCameraIndex].GetComponent<Camera>().name +
                          ", is now enabled");
            }
        }
    }

    public static void ResetCameras() {
        foreach (GameObject camera in cameras) {
            camera.SetActive(false);
        }
        cameras[0].SetActive(true);
    }
}