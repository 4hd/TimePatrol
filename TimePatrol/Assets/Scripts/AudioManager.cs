﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{

    public Sound[] sounds;

    public static AudioManager instance;

    public Slider VolSlider;


    void Awake()
    {
        /*if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        
        DontDestroyOnLoad(gameObject);*/
        
        
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            
            //s.source.volume = s.volume = VolSlider.value;

        }
    }

    void Start()
    {
        Play("Theme");    //Name of the song played (in Unity)
    }
    

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound :" + name + " not found (check spelling)");
            return;
        }
        s.source.Play();
    }

    /*public class VolumeValueChange : MonoBehaviour {

        // Reference to Audio Source component
        private AudioSource audioSrc;

        // Music volume variable that will be modified
        // by dragging slider knob
        private float musicVolume = 1f;

        // Use this for initialization
        void Start () {

            // Assign Audio Source component to control it
            audioSrc = GetComponent<AudioSource>();
        }
	
        // Update is called once per frame
        void Update () {

            // Setting volume option of Audio Source to be equal to musicVolume
            audioSrc.volume = musicVolume;
        }

        // Method that is called by slider game object
        // This method takes vol value passed by slider
        // and sets it as musicValue
        public void SetVolume(float vol)
        {
            musicVolume = vol;
        }
    }*/
}

    
