﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class Dialogue
{
    [TextArea(3, 10)] 
    public List<string> sentences;

    public void SentenceParser(string fileName)
    {
        string text = new StreamReader(fileName).ReadToEnd();
        string sentence = "";

        int len = text.Length;
        
        for (int i = 0; i < len; i++)
        {
            if (text[i] == '.' || text[i] == '!' || text[i] == '?')
            {
                sentences.Add(sentence + text[i]);
                sentence = "";
            }
            
            else
                sentence += text[i];
        }
    }
}