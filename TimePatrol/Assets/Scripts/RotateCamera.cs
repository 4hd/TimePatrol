﻿﻿using UnityEngine;

public class RotateCamera : MonoBehaviour
{

    //Can Use Touch
    bool isTouchDevice;

    //Speed
    float arrowMouseSpeed = 1.0f;

    // Use this for initialization
    void Start()
    {
        //check if touch device
        if (SystemInfo.deviceType == DeviceType.Handheld)
        {
            isTouchDevice = true;
        }
        else
        {
            isTouchDevice = false;
        }

        //Get local rotation
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;

    }

    // Update is called once per frame
    void Update()
    {
        
        {
            //Rotate Camera with Keyboard Arrow and Mouse
            moveWithArrowAndMouse();
        }
    }

    

    //Move with Keyboard Arrow
    void moveWithArrowAndMouse()
    {
        //Keyboard Arrow
        moveCamera(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), arrowMouseSpeed);

        //Keyboard Mouse
        moveCamera(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), arrowMouseSpeed);
    }

    //Move Parameters
    float mouseX;
    float mouseY;
    Quaternion localRotation;
    private float rotY = 0.0f; // rotation around the up/y axis
    private float rotX = 0.0f; // rotation around the right/x axis

    void moveCamera(float horizontal, float verticle, float moveSpeed)
    {
        mouseX = horizontal;
        mouseY = -verticle;

        rotY += mouseX * moveSpeed;
        rotX += mouseY * moveSpeed;

        localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = localRotation;
    }
}
