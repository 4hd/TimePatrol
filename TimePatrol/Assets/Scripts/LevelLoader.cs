﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour {
    public GameObject loadingScreen;
    public Slider slider;
    public Text progressText;


    public void LoadLevel(int sceneIndex) {
        if(sceneIndex >= 0)
            StartCoroutine(LoadAsynchronously(sceneIndex));
        else {
            GameObject.Find("Load").SetActive(false);
            StartCoroutine(LoadAsynchronously(Load.scene + 4));
        }
    }

    IEnumerator LoadAsynchronously(int sceneIndex) {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);


        loadingScreen.SetActive(true);


        while (!operation.isDone) {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            slider.value = progress;

            progressText.text = progress * 100 + "%";

            yield return null;
        }
    }
}