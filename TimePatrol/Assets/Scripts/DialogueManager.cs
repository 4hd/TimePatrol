﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public Text dialogueText;
    private Queue<string> sentences;
    
    // Start is called before the first frame update
    void Start()
    {
        sentences = new Queue<string>();
    }

    public void StartDialogue(Queue<string> sentences)
    {
        this.sentences = sentences;
        
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
            EndDialogue();
        
        else
        {
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentences.Dequeue()));
        }
    }

    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence)
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    public void EndDialogue()
    {
        dialogueText.text = "END OF TRANSMISSION\n\n\nPRESS [enter] TO EXIT\nTHE SPACETIME TRANSCEIVER";
        StopAllCoroutines();
        StartCoroutine(TypeSentence(dialogueText.text));
        Debug.Log("END OF TRANSMISSION");
    }
}
