﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EA
{
    public class Tile
    {

        private Vector2Int pos = new Vector2Int();//ligne a remodifier?
        private int type; //skin?
        public List<Tile> adjacentTiles = new List<Tile>();
        private int mobility; //cout
        private Unit unit;

        public Tile(int type, int x, int y)
        {
            this.type = type;
            mobility = type;

            pos.x = x;
            pos.y = y;

            unit = null;
        }

        public Vector2Int GetPos()
        {
            return pos;
        }

        public void SetType(int type)
        {
            this.type = type;
        }

        public int TypeReturn()
        {
            return type;
        }

        public List<Tile> GetAdjacent() {
            return adjacentTiles;
        }

        public int MobRequirered() {
            return mobility;
        }

        public Unit GetUnit() {
            return unit;
        }

        public void SetUnit(Unit unit) {
            this.unit = unit;
        }
    }
}