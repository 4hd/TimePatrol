using System;
using System.Collections.Generic;

namespace EA {
    public class Print {
        
        public static void Map(Maps m) {
            ConsoleColor defB = Console.BackgroundColor;
            ConsoleColor defF = Console.ForegroundColor;
            
            foreach (var t in m.GetTileList()) {
                Console.SetCursorPosition(t.GetPos().x + 1, t.GetPos().y + 1);
                switch (t.MobRequirered()) {
                    case 0:
                        Console.BackgroundColor = ConsoleColor.Black;
                        break;
                    case 1:
                        Console.BackgroundColor = ConsoleColor.Green;
                        break;
                    case 2:
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                        break;
                    case 3:
                        Console.BackgroundColor = ConsoleColor.Gray;
                        break;
                    case 4:
                        Console.BackgroundColor = ConsoleColor.Blue;
                        break;
                    default:
                        Console.BackgroundColor = ConsoleColor.White;
                        break;
                }

                if (t.GetUnit() != null) {
                    switch (t.GetUnit().GetTeam()) {
                        case "blue":
                            Console.ForegroundColor = ConsoleColor.DarkBlue;
                            break;
                        case "red":
                            Console.ForegroundColor = ConsoleColor.Red;
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.White;
                            break;
                    }

                    if (t.GetUnit() is Melee) {
                        Console.Write('M');
                    }

                    if (t.GetUnit() is Tank) {
                        Console.Write('T');
                    }

                    if (t.GetUnit() is Range) {
                        Console.Write('R');
                    }

                    if (t.GetUnit() is Special) {
                        Console.Write('S');
                    }
                }
                else {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(' ');
                }
            }

            Console.BackgroundColor = defB;
            Console.ForegroundColor = defF;
        }

        public static void Cursor(Vector2Int currsor, Maps m) {
            ConsoleColor def = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Black;
            int width = m.GetWidth();
            int height = m.GetHeight();
            
            //clear cursors
            for (int i = 1; i <= width; i++) {
                Console.SetCursorPosition(i,0);
                Console.Write(' ');
            }
            for (int i = 1; i <= height; i++) {
                Console.SetCursorPosition(0,i);
                Console.Write(' ');
            }
            
            Console.SetCursorPosition(0,currsor.y + 1);
            Console.Write('>');
            Console.SetCursorPosition(currsor.x + 1, 0);
            Console.Write('V');
            Console.ForegroundColor = def;
        }

        public static void Info(Maps m, Tile t) {
            int width = m.GetWidth();
            int height = m.GetHeight();
            
            //clear info
            Console.SetCursorPosition(0, height + 2);
            for (int i = height + 1; i < height + 20; i++) {
                for (int j = 0; j < 25; j++) {
                    Console.SetCursorPosition(j,i);
                    Console.Write(' ');
                }
            }
            
            ConsoleColor def = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            
            Console.SetCursorPosition(0, height + 2);
            Console.WriteLine("X: "+t.GetPos().x);
            Console.WriteLine("Y: "+t.GetPos().y);
            Console.WriteLine("Cost: "+t.MobRequirered());
            Unit u = t.GetUnit();
            if (u != null) {
                Console.WriteLine("\nUnit: ");
                Console.WriteLine("Health: "+u.GetHealth());
            }

            Console.ForegroundColor = def;
        }

        public static void Movement(Maps m, List<Tile> l) {
            ConsoleColor defB = Console.BackgroundColor;
            ConsoleColor defF = Console.ForegroundColor;
            
            foreach (Tile t in l) {
                if (t.GetUnit() == null) {
                    switch (t.MobRequirered()) {
                        case 0:
                            Console.BackgroundColor = ConsoleColor.Black;
                            break;
                        case 1:
                            Console.BackgroundColor = ConsoleColor.Green;
                            break;
                        case 2:
                            Console.BackgroundColor = ConsoleColor.DarkGreen;
                            break;
                        case 3:
                            Console.BackgroundColor = ConsoleColor.Gray;
                            break;
                        case 4:
                            Console.BackgroundColor = ConsoleColor.Blue;
                            break;
                        default:
                            Console.BackgroundColor = ConsoleColor.White;
                            break;
                    }

                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write('#');
                }
            }

            Console.ForegroundColor = defF;
            Console.BackgroundColor = defB;
        }
    }
}