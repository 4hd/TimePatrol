using System;
using System.Collections.Generic;
using System.Linq;

namespace EA
{
    public class Maps
    {
        
        Dictionary<Vector2Int, Tile> tileDict;
        List<Tile> tileList;
        List<Unit> unitList1;
        List<Unit> unitList2;
        int width;
        int heigth;
        
        public Maps(string name) {
            int[,] mapData = OpenMap(name);

            width = mapData.GetLength(0);
            heigth = mapData.GetLength(1);

            tileDict = new Dictionary<Vector2Int, Tile>();
            tileList = new List<Tile>();
            
            CreateTiles(mapData);

            unitList1 = new List<Unit>();
            unitList2 = new List<Unit>();
            
            SpawnUnits(name);
        }

        public Maps(string name, string team1, string team2) {
            int[,] mapData = OpenMap(name);

            width = mapData.GetLength(0);
            heigth = mapData.GetLength(1);

            tileDict = new Dictionary<Vector2Int, Tile>();
            tileList = new List<Tile>();
            
            CreateTiles(mapData);

            unitList1 = new List<Unit>();
            unitList2 = new List<Unit>();
            
            SpawnUnits(name, team1, team2);
        }

        public List<Tile> GetTileList() {
            return tileList;
        }

        public List<Unit> GetTeam1() {
            return unitList1;
        }
        
        public List<Unit> GetTeam2() {
            return unitList2;
        }

        public int[,] OpenMap(string mapName) {
            string[] tmp = Outils.FileToString("Map " + mapName + ".lvl");

            List<string> mapList = new List<string>(); //cree la partie map
            int i = 0;
            while (i < tmp.Length && tmp[i] != "#") {
                mapList.Add(tmp[i]);
                i++;
            }

            return Outils.StringToIntArray(Outils.StringListToArray(mapList));
        }

        public void CreateTiles(int[,] mapData) //cree chaque case et les mets dans la liste tileList et le dictionnaire tileDict
        {
            for(int y = 0; y < mapData.GetLength(1); y++)
            {
                for(int x = 0; x < mapData.GetLength(0); x++)
                {
                    Tile t = new Tile(mapData[x, y], x, y);
                    tileList.Add(t);
                    tileDict.Add(t.GetPos(), t);
                }
            }

            foreach(KeyValuePair<Vector2Int, Tile> e in tileDict) //cherche les cases adjacentes de chaque cases et les ajoutent a Tile.adjacentTiles
            {
                Tile t = e.Value;
                Tile u;

                Vector2Int up = new Vector2Int(); //à suppr?
                up.x = e.Key.x;
                up.y = e.Key.y + 1;
                u = FindTile(up);
                if(u != null) {t.adjacentTiles.Add(u);}
                
                Vector2Int down = new Vector2Int();
                down.x = e.Key.x;
                down.y = e.Key.y - 1;
                u = FindTile(down);
                if(u != null) {t.adjacentTiles.Add(u);}
                
                Vector2Int left = new Vector2Int();
                left.x = e.Key.x - 1;
                left.y = e.Key.y;
                u = FindTile(left);
                if(u != null) {t.adjacentTiles.Add(u);}
                
                Vector2Int right = new Vector2Int();
                right.x = e.Key.x + 1;
                right.y = e.Key.y;
                u = FindTile(right);
                if(u != null) {t.adjacentTiles.Add(u);}
            }
        }
        
        public void SpawnUnits(string mapName, string team1 = "past", string team2 = "past") {
            string[] tmp = Outils.FileToString("Map " + mapName + ".lvl");
            
            int i = 0;
            //permet de passer la partie cases
            while (i < tmp.Length && tmp[i] != "#") { 
                i++;
            }
            i++;

            //recupere les donnees de spawn de l'equipe 1 (n0)
            while (i < tmp.Length && tmp[i] != "@") { 
                int j = 2;
                int x = 0;
                int y = 0;
                //recupere x
                while (j < tmp[i].Length && tmp[i][j] != ' ') { 
                    int tmpX;
                    if (int.TryParse(tmp[i][j]+"", out tmpX)) {
                        x *= 10;
                        x += tmpX;
                    }
                    j++;
                }

                j++;
                //recupere y
                while (j < tmp[i].Length && tmp[i][j] != ' ') { 
                    int tmpY;
                    if (int.TryParse(tmp[i][j]+"", out tmpY)) {
                        y *= 10;
                        y += tmpY;
                    }
                    j++;
                }


                Unit u;
                //cree unite et la met dans unitList
                switch (team1) {
                    case "past":
                        switch (tmp[i][0]) {
                            case 'M':
                                u = new Melee.Knight();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                            case 'R':
                                u = new Range.CrossbowMan();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                            case 'T':
                                u = new Tank.Ursari();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                            case 'S':
                                u = new Special.Wizard();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                        }
                        break;
                    case "now":
                        switch (tmp[i][0]) {
                            case 'M':
                                u = new Melee.Infantry();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                            case 'R':
                                u = new Range.Gunner();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                            case 'T':
                                u = new Tank.AntiRiot();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                            case 'S':
                                u = new Special.Medic();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                        }
                        break;
                    case "future":
                        switch (tmp[i][0]) {
                            case 'M':
                                u = new Melee.Mecha();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                            case 'R':
                                u = new Range.RobotSniper();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                            case 'T':
                                u = new Tank.AssaultTank();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                            case 'S':
                                u = new Special.Cyborg();
                                u.SetPos(x,y);
                                unitList1.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("blue");
                                break;
                        }
                        break;
                }
                
                
                i++;
            }
            
            
            //recupere les donnees de spawn de l'equipe 2
            while (i < tmp.Length) { 
                int j = 2;
                int x = 0;
                int y = 0;
                //recupere x
                while (j < tmp[i].Length && tmp[i][j] != ' ') { 
                    int tmpX;
                    if (int.TryParse(tmp[i][j]+"", out tmpX)) {
                        x *= 10;
                        x += tmpX;
                    }
                    j++;
                }

                j++;
                //recupere y
                while (j < tmp[i].Length && tmp[i][j] != ' ') { 
                    int tmpY;
                    if (int.TryParse(tmp[i][j]+"", out tmpY)) {
                        y *= 10;
                        y += tmpY;
                    }
                    j++;
                }
                
                Unit u;
                //cree unite et la met dans unitList
                switch (team2) {
                    case "past":
                        switch (tmp[i][0]) {
                            case 'M':
                                u = new Melee.Knight();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                            case 'R':
                                u = new Range.CrossbowMan();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                            case 'T':
                                u = new Tank.Ursari();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                            case 'S':
                                u = new Special.Wizard();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                        }
                        break;
                    case "now":
                        switch (tmp[i][0]) {
                            case 'M':
                                u = new Melee.Infantry();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                            case 'R':
                                u = new Range.Gunner();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                            case 'T':
                                u = new Tank.AntiRiot();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                            case 'S':
                                u = new Special.Medic();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                        }
                        break;
                    case "future":
                        switch (tmp[i][0]) {
                            case 'M':
                                u = new Melee.Mecha();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                            case 'R':
                                u = new Range.RobotSniper();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                            case 'T':
                                u = new Tank.AssaultTank();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                            case 'S':
                                u = new Special.Cyborg();
                                u.SetPos(x,y);
                                unitList2.Add(u);
                                foreach (Tile t in tileList) {
                                    if (t.GetPos().x == x && t.GetPos().y == y) {
                                        t.SetUnit(u);
                                    }
                                }
                                u.SetTeam("red");
                                break;
                        }
                        break;
                } 
                i++;
            } 
            
        }

        public List<Tile> Movement(Unit u) //renvoie une liste ou le joueur peut se deplacer s'il est sur la carte
        {
            Vector2Int pos = u.GetPos(); //modif??

            Tile t = FindTile(pos);
            if(t != null) {
                return Walkable(t, u.GetMobility());
            }
            else {
                return null;
            }
        }

        private List<Tile> Walkable(Tile curr, int mobi) { //revoie la liste des cases avec deplacement valide
            if (mobi > 0) {
                List<Tile> tiles = new List<Tile>();
                foreach (var e in curr.GetAdjacent()) {
                    if (e.MobRequirered() <= mobi) {
                        tiles.Add(e);
                    }
                }
                
                List<Tile> res = new List<Tile>();
                res.Add(curr);
                foreach (var e in tiles) {
                    Concatenate(res, Walkable(e, mobi - e.MobRequirered()));
                }

                return res;
            }
            else {
                return new List<Tile>();
            }
        }

        private void Concatenate(List<Tile> liste1, List<Tile> liste2) { //concatene 2 listes et les met dans liste1
            foreach (var e2 in liste2) {
                if (!liste1.Contains(e2)) {
                    liste1.Add(e2);
                }
            }
        }

        public bool ChangeUnitPos(Unit u, Tile t) {
            if(tileList.Contains(t) && Movement(u).Contains(t)) {
                u.SetPosVect(t.GetPos());
                return true;
            }
            return false;
        }

        public Tile FindTile(Vector2Int v) {
            foreach (Tile t in tileList) {
                if (v.x == t.GetPos().x && v.y == t.GetPos().y) {
                    return t;
                }
            }

            return null;
        }

        public int GetWidth() {
            return width;
        }

        public int GetHeight() {
            return heigth;
        }
    }
}