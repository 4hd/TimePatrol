using System.Runtime.CompilerServices;

namespace EA
{
    public class Melee : Unit
    {
        public class Knight : Melee
        {
            public Knight()
            {
                health = 100; 
                attack = 70;
                defense = 25;
                mobility = 8;
                attack_range = 1;
                speed = 35;
                dodge = 10;
                luck = 10;
            }
        }
        
        public class Infantry : Melee
        {
            public Infantry()
            {
                health = 80; 
                attack = 55;
                defense = 22;
                mobility = 9;
                attack_range = 1;
                speed = 45;
                dodge = 15;
                luck = 15;
            }
        }
        
        public class Mecha : Melee
        {
            public Mecha()
            {
                health = 70; 
                attack = 40;
                defense = 22;
                mobility = 13;
                attack_range = 1;
                speed = 70;
                dodge = 35;
                luck =  30;
            }
        }
    }
}