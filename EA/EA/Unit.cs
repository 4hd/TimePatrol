using System.Runtime.InteropServices;

namespace EA
{
    public abstract class Unit
    {
        protected int health;
        protected int attack;
        protected int defense;
        protected int mobility;
        protected int attack_range;
        protected int speed;
        protected int dodge;
        protected int luck;
        protected string team;
        protected Vector2Int pos;

        public void SetTeam(string team) {
            this.team = team;
        }

        public string GetTeam() {
            return team;
        }

        public void SetPos(int x, int y) {
            Vector2Int pos = new Vector2Int();
            pos.x = x;
            pos.y = y;
            this.pos = pos;
        }

        public void SetPosVect(Vector2Int pos) {
            this.pos = pos;
        }

        public Vector2Int GetPos()
        {
            return pos;
        }

        public int GetMobility()
        {
            return mobility;
        }
        
        public int GetHealth()
        {
            return health;
        }
        
        public int GetAttack()
        {
            return attack;
        }
        
        public int GetDefense()
        {
            return defense;
        }
        
        public int GetAttack_range()
        {
            return attack_range;
        }
        
        public int GetSpeed()
        {
            return speed;
        }
        
        public int GetDodge()
        {
            return dodge;
        }

        public int GetLuck()
        {
            return luck;
        }

        public void RemoveHealth(int health)
        {
            this.health -= health;
        }
        
        
    }
}
