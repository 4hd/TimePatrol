using System;
using System.Data.SqlTypes;
using System.Xml;

namespace EA
{
    public class Battle : Unit {
        public int Calc_DMG(Unit atker, Unit defer, bool crit) {
            int ATK = 0; //Rider ne trouvait pas ATK sans ça
            if (crit) {
                ATK = atker.GetAttack() * 2;
            }
            else {
                ATK = atker.GetAttack();
            }

            int DEF = defer.GetDefense();

            return ATK - DEF;
        }

        // appeler RemoveHealth avec en attribut la valeur de Calc_DMG

        public bool Double_hit(Unit atker, Unit defer) {
            int speed_atker = atker.GetSpeed();
            int speed_defer = defer.GetSpeed();

            return speed_atker >= 1.5 * speed_defer;
        }

        public bool Dodge(Unit defer) {
            int dodge = defer.GetDodge();
            Random rand = new Random();
            int hit_prob = rand.Next(0, 100);

            return (hit_prob <= dodge);

        }

        public bool Crit(Unit atker) {
            int crit = atker.GetLuck();
            Random rand = new Random();
            int crit_prob = rand.Next(0, 100);

            return (crit_prob <= crit);
        }

        public bool Combat(Unit atker, Unit defer) {
            if (!Dodge(defer)) {
                RemoveHealth(Calc_DMG(atker, defer, Crit(atker)));
                return true;
            }
            return false;

            // trouver comment matérialiser l'esquive 
            // on peut esquiver le premier coup et se prendre le deuxième donc peut etre penser à appeler
            // double_hit en degors de combat au lieu de refaire encore une fois la même boucle
        }
    }
}