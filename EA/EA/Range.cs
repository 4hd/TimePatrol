namespace EA {
    public class Range : Unit {
        public class CrossbowMan : Range {
            public CrossbowMan() {
                health = 60;
                attack = 50;
                defense = 15;
                mobility = 5;
                attack_range = 2;
                speed = 35;
                dodge = 20;
                luck = 35;
            }
        }

        public class Gunner : Range {
            public Gunner() {
                health = 55;
                attack = 45;
                defense = 17;
                mobility = 5;
                attack_range = 2;
                speed = 45;
                dodge = 20;
                luck = 20;
            }
        }

        public class RobotSniper : Range {
            public RobotSniper() {
                health = 50;
                attack = 40;
                defense = 15;
                mobility = 7;
                attack_range = 3; //or 4 
                speed = 55;
                dodge = 35;
                luck = 30;
            }
        }
    }
}