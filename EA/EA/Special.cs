namespace EA
{
    public class Special : Unit
    {
        public class Wizard : Special
        {
            public Wizard()
            {
                health = 50; 
                attack = 60;
                defense = 10;
                mobility = 5;
                attack_range = 2;
                speed = 45;
                dodge = 5;
                luck = 5;
            }
        }
        
        public class Medic : Special
        {
            public Medic()
            {
                health = 60; 
                attack = 20;
                defense = 10;
                mobility = 6;
                attack_range = 1;
                speed = 45;
                //healing
                dodge = 10;
                luck = 10;
            }
        }
        
        public class Cyborg : Special
        {
            public Cyborg()
            {
                health = 90; 
                attack = 65;
                defense = 22;
                mobility = 6;
                attack_range = 2;
                speed = 55;
                dodge = 25;
                luck = 20;
            }
        }
        
        
    }
}