using System;
using System.Collections.Generic;

namespace EA {
    public class _Game {
        public static void Update(Maps m, bool player1 = true) {
            Tile t = SelectTile(m);
            Unit u = t.GetUnit();
            
            if (u != null) {
                List<Tile> mov = m.Movement(u); //a debug
                Print.Movement(m, mov); //??????????
                Update(m, !player1);
            }
            else {
                Update(m, player1);
            }
        }

        public static Tile SelectTile(Maps m) {
            Vector2Int cursor = new Vector2Int();
            cursor.x = 0;
            cursor.y = 0;
            ConsoleKey pressed;
            Tile selected;
            Console.Clear();
            Print.Map(m);
            do {
                Print.Cursor(cursor, m);
                selected = m.FindTile(cursor);
                Print.Info(m, selected);

                pressed = Console.ReadKey().Key;
                switch (pressed) {
                    case ConsoleKey.LeftArrow:
                        cursor.x = (cursor.x - 1 + m.GetWidth()) % m.GetWidth();
                        break;
                    case ConsoleKey.RightArrow:
                        cursor.x = (cursor.x + 1) % m.GetWidth();
                        break;
                    case ConsoleKey.UpArrow:
                        cursor.y = (cursor.y - 1 + m.GetHeight()) % m.GetHeight();
                        break;
                    case ConsoleKey.DownArrow:
                        cursor.y = (cursor.y + 1) % m.GetHeight();
                        break;
                }
            } while (pressed != ConsoleKey.Enter);

            return selected;
        }
    }
}